//bot.data[info.serverId].modules

module.exports =
{
    commands:
        [
            {
                cmd: "infect",
                params: "Choose a character to infect",
                options: [{ type: 3, name: "character", description: "The character to infect!", required: true }, { type: 3, name: "severity", description: "The severity of the infection!", required: false }],
                execute: function (mdata, bot, info, args, options)
                {
                    var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];

                    if (character)
                    {
                        mdata[character.Name] = mdata[character.Name] || {};
                        mdata[character.Name].infection = mdata[character.Name].infection || 0;

                        console.log(mdata[character.Name]);

                        if (options[1])
                        {
                            let level = options[1].value.trim().toLowerCase();

                            switch (level)
                            {
                                case "total": mdata[character.Name].infection = 100; break;
                                case "near-total": mdata[character.Name].infection = 81; break;
                                case "extreme": mdata[character.Name].infection = 61; break;
                                case "major": mdata[character.Name].infection = 41; break;
                                case "moderate": mdata[character.Name].infection = 31; break;
                                case "minor": mdata[character.Name].infection = 21; break;
                                case "trivial": mdata[character.Name].infection = 11; break;
                                case "negligible": mdata[character.Name].infection = 1; break;
                            }
                        }
                        else
                        {
                            mdata[character.Name].infection = 1;
                        }

                        let percentage = mdata[character.Name].infection;
                        let label = "None";
                        if (percentage > 0)
                            label = "Negligible";
                        if (percentage > 10)
                            label = "Trivial";
                        if (percentage > 20)
                            label = "Minor";
                        if (percentage > 30)
                            label = "Moderate";
                        if (percentage > 40)
                            label = "Major";
                        if (percentage > 60)
                            label = "Extreme";
                        if (percentage > 80)
                            label = "Near-total";
                        if (percentage > 99)
                            label = "Total";

                        bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, "Infection: " + label, null, true, "Infected!");
                    }
                    else
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Character '" + character.Name + "' not found!",
                            typing: false
                        });
                    }

                    info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 2000);
                    });
                }
            },
            {
                cmd: "vaccinate",
                params: "Choose a character to vaccinate!",
                options: [{ type: 3, name: "character", description: "The character to infect!", required: true }],
                execute: function (mdata, bot, info, args, options)
                {
                    var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];

                    let postOK = true;

                    if (character)
                    {
                        mdata[character.Name] = mdata[character.Name] || {};
                        mdata[character.Name].infection = mdata[character.Name].infection || 0;

                        console.log(mdata[character.Name]);
                        if (mdata[character.Name].infection <= 40)
                        {
                            if (mdata[character.Name].infection > 0)
                                mdata[character.Name].infection = 1;

                            bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, "Infection: Negligible", null, true, "Infection Reduced!");
                        }
                        else
                        {
                            postOK = false;
                            info.evt.interaction && info.evt.interaction.reply({ content: 'Target infection level too high to vaccinate.' });
                        }
                    }
                    else
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Character '" + character.Name + "' not found!",
                            typing: false
                        });
                    }

                    if (postOK)
                    {
                        info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                        {
                            setTimeout(() =>
                            {
                                info.evt.interaction.deleteReply();
                            }, 2000);
                        });
                    }
                }
            },
            {
                cmd: "cure",
                params: "Choose a character to completely cure!",
                options: [{ type: 3, name: "character", description: "The character to cure!", required: true }],
                execute: function (mdata, bot, info, args, options)
                {
                    var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                    let postOK = true;
                    if (character)
                    {
                        mdata[character.Name] = mdata[character.Name] || {};
                        mdata[character.Name].infection = mdata[character.Name].infection || 0;

                        if (mdata[character.Name].infection <= 20)
                        {
                            mdata[character.Name].infection = 0;

                            bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, "Infection: None", null, true, "Infection Cured!");
                        }
                        else
                        {
                            postOK = false;
                            info.evt.interaction && info.evt.interaction.reply({ content: 'Target infection level too high to cure. Try `/vaccinate` first!' })
                        }
                    }
                    else
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Character '" + options[0].value + "' not found!",
                            typing: false
                        });
                    }

                    if (postOK)
                    {
                        info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                        {
                            setTimeout(() =>
                            {
                                info.evt.interaction.deleteReply();
                            }, 2000);
                        });
                    }
                }
            },
            {
                cmd: "forcecure",
                params: "Choose a character to completely cure!",
                options: [{ type: 3, name: "character", description: "The character to cure!", required: true }],
                hidden: true,
                execute: function (mdata, bot, info, args, options)
                {
                    var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];

                    if (character)
                    {
                        mdata[character.Name] = mdata[character.Name] || {};
                        mdata[character.Name].infection = mdata[character.Name].infection || 0;

                        mdata[character.Name].infection = 0;

                        bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, "Infection: None", null, true, "Infection Cured!");
                    }
                    else
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Character '" + options[0].value + "' not found!",
                            typing: false
                        });
                    }

                    info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 2000);
                    });
                }
            },
            {
                cmd: "immunity",
                params: "Set the immunity level of a character to either full, partial or none!",
                options: [{ type: 3, name: "character", description: "The character to configure!", required: true }, { type: 3, name: "immunitytype", description: "Full/Partial/None", required: true }],
                execute: function (mdata, bot, info, args, options)
                {
                    var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];

                    if (character)
                    {
                        mdata[character.Name] = mdata[character.Name] || {};
                        mdata[character.Name].infection = mdata[character.Name].infection || 0;

                        mdata[character.Name].infection = 0;

                        mdata[character.Name].immunity = options[1].value.toLowerCase().trim() == "true" || options[1].value.toLowerCase().trim() == "full";
                        mdata[character.Name].partialImmunity = options[1].value.toLowerCase().trim() == "partial";

                        let text = "Infection immunity set to: ```diff\n"
                            + (mdata[character.Name].immunity ? "+" : "-") + " Full Immunity (cannot have an infection status at all)\n"
                            + (mdata[character.Name].partialImmunity ? "+" : "-") + " Partial Immunity (control over when to use Chaos Attacks)"
                            + "\n```";

                        bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, "Immunity", null, true, text);
                    }
                    else
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Character '" + character.Name + "' not found!",
                            typing: false
                        });
                    }


                    info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 2000);
                    });
                }
            }
        ],

    infectionLevelText: function (percentage)
    {
        if (percentage > 99)
            return "Total";
        if (percentage > 80)
            return "Near-total";
        if (percentage > 60)
            return "Extreme";
        if (percentage > 40)
            return "Major";
        if (percentage > 30)
            return "Moderate";
        if (percentage > 20)
            return "Minor";
        if (percentage > 10)
            return "Trivial";
        if (percentage > 0)
            return "Negligible";

        return "None";
    },

    onSpeak: function (mdata, bot, character, info, text)
    {
        let infectCmd = false;
        mdata[character.Name] = mdata[character.Name] || {};
        mdata[character.Name].infection = mdata[character.Name].infection || 0;
        let args = text.split(":");
        if (args.length == 2 && args[0].toLowerCase().trim() == "infect")
        {
            mdata[character.Name].infection = parseInt(args[1]);
            infectCmd = true;
        }

        if (args.length == 2 && args[0].toLowerCase().trim() == "immune")
        {
            mdata[character.Name].immunity = args[1].toLowerCase().trim() == "true" || args[1].toLowerCase().trim() == "full";
            mdata[character.Name].partialImmunity = args[1].toLowerCase().trim() == "partial";
            infectCmd = true;
        }

        return {
            extraText: infectCmd ? "Infection set to: " + this.infectionLevelText(mdata[character.Name].infection) + "```diff\n"
                + (mdata[character.Name].immunity ? "+" : "-") + " Full Immunity (cannot have an infection status at all)\n"
                + (mdata[character.Name].partialImmunity ? "+" : "-") + " Partial Immunity (control over when to use Chaos Attacks)"
                + "\n```" : ""
        };
    },

    onAttack: function (mdata, bot, character, info, target, wasChaos, hit, damage)
    {
        mdata[character.Name] = mdata[character.Name] || {};
        mdata[target.Name] = mdata[target.Name] || {};
        mdata[character.Name].infection = mdata[character.Name].infection || 0;
        mdata[target.Name].infection = mdata[target.Name].infection || 0;

        if (mdata[character.Name].infection > 0)
        {
            if (mdata[character.Name].infection > 40 && !mdata[character.Name].partialImmunity)
                wasChaos = true;

            mdata[character.Name].infection += mdata[character.Name].infection * 0.1;

            if (wasChaos)
                mdata[character.Name].infection += character.CHAOS;

            mdata[character.Name].infection = Math.min(100, Math.max(0, mdata[character.Name].infection));
            mdata[target.Name].infection = Math.min(100, Math.max(0, mdata[target.Name].infection));

            if (mdata[character.Name].immunity)
                mdata[character.Name].infection = 0;
        }
        return {
            extraText: mdata[character.Name].infection > 0 ? "Infection: " + this.infectionLevelText(mdata[character.Name].infection) : null,
            wasChaos: wasChaos
        };
    },

    onDefend: function (mdata, bot, character, info, attacker, wasChaos, hit, damage)
    {
        mdata[character.Name] = mdata[character.Name] || {};
        mdata[attacker.Name] = mdata[attacker.Name] || {};
        mdata[character.Name].infection = mdata[character.Name].infection || 0;
        mdata[attacker.Name].infection = mdata[attacker.Name].infection || 0;

        if (mdata[character.Name].infection > 0 || mdata[attacker.Name].infection > 0)
        {
            if (wasChaos && hit)
                mdata[character.Name].infection += character.CHAOS;
        }

        if (hit && mdata[character.Name].infection < mdata[attacker.Name].infection)
        {
            mdata[character.Name].infection += (mdata[attacker.Name].infection - mdata[character.Name].infection) * 0.07;
        }

        mdata[character.Name].infection = Math.min(100, Math.max(0, mdata[character.Name].infection));
        mdata[attacker.Name].infection = Math.min(100, Math.max(0, mdata[attacker.Name].infection));

        if (mdata[character.Name].immunity)
            mdata[character.Name].infection = 0;

        return {
            extraText: mdata[character.Name].infection > 0 ? "Infection: " + this.infectionLevelText(mdata[character.Name].infection) : null,
        };
    },

    onHeal: function (mdata, bot, character, info, target, healedPoints)
    {
        mdata[character.Name] = mdata[character.Name] || {};
        mdata[target.Name] = mdata[target.Name] || {};
        mdata[character.Name].infection = mdata[character.Name].infection || 0;
        mdata[target.Name].infection = mdata[target.Name].infection || 0;

        if (mdata[character.Name].infection < mdata[target.Name].infection)
        {
            mdata[character.Name].infection += (mdata[target.Name].infection - mdata[character.Name].infection) * 0.07;
        }

        mdata[character.Name].infection = Math.min(100, Math.max(0, mdata[character.Name].infection));
        mdata[target.Name].infection = Math.min(100, Math.max(0, mdata[target.Name].infection));

        if (mdata[character.Name].immunity)
            mdata[character.Name].infection = 0;

        return {
            extraText: mdata[character.Name].infection > 0 ? "Infection: " + this.infectionLevelText(mdata[character.Name].infection) : null,
        };
    },

    onHealed: function (mdata, bot, character, info, healer, healedPoints)
    {
        mdata[character.Name] = mdata[character.Name] || {};
        mdata[healer.Name] = mdata[healer.Name] || {};
        mdata[character.Name].infection = mdata[character.Name].infection || 0;
        mdata[healer.Name].infection = mdata[healer.Name].infection || 0;

        if (mdata[character.Name].infection < mdata[healer.Name].infection)
        {
            mdata[character.Name].infection += (mdata[healer.Name].infection - mdata[character.Name].infection) * 0.1;
        }

        mdata[character.Name].infection = Math.min(100, Math.max(0, mdata[character.Name].infection));
        mdata[healer.Name].infection = Math.min(100, Math.max(0, mdata[healer.Name].infection));

        if (mdata[character.Name].immunity)
            mdata[character.Name].infection = 0;

        return {
            extraText: mdata[character.Name].infection > 0 ? "Infection: " + this.infectionLevelText(mdata[character.Name].infection) : null,
        };
    },

    onBuff: function (mdata, bot, character, info, target, strength)
    {
        return {};
    },

    onBuffed: function (mdata, bot, character, info, buffer, strength)
    {
        return {};
    },

    onBoost: function (mdata, bot, character, info, target, strength)
    {
        return {};
    },

    onBoosted: function (mdata, bot, character, info, target, strength)
    {
        return {};
    },

    onWard: function (mdata, bot, character, info, target, strength)
    {
        return {};
    },

    onWarded: function (mdata, bot, character, info, target, strength)
    {
        return {};
    },

    onCheck: function (mdata, bot, character, info, stat, roll)
    {
        return {};
    },

    onInitiative: function (mdata, bot, character, info, roll)
    {
        mdata[character.Name] = mdata[character.Name] || {};
        mdata[character.Name].infection = mdata[character.Name].infection || 0;
        return { roll: roll + Math.ceil(mdata[character.Name].infection * 0.1) };
    },

    onDetails: function (mdata, bot, character, info)
    {
        mdata[character.Name] = mdata[character.Name] || {};
        mdata[character.Name].infection = mdata[character.Name].infection || 0;
        mdata[character.Name].infection = Math.min(100, Math.max(0, mdata[character.Name].infection));

        if (mdata[character.Name].immunity)
            mdata[character.Name].infection = 0;

        return {
            extraText: mdata[character.Name].infection > 0 ? "Infection: " + this.infectionLevelText(mdata[character.Name].infection) : null,
        };
    },

    overwriteChaosName: function (mdata, bot, character, info, originalName)
    {
        mdata[character.Name] = mdata[character.Name] || {};
        mdata[character.Name].infection = mdata[character.Name].infection || 0;

        mdata[character.Name].infection = Math.min(100, Math.max(0, mdata[character.Name].infection));
        return (mdata[character.Name].infection > 20 && !mdata[character.Name].partialImmunity) ? "CHAOS CORRUPTION" : originalName;
    }
};