var fs = require('fs');
const rando = require('math-random');
const axios = require("axios");
var auth = require('./auth.json');
var rpmodules = require('./rp-modules');
const { resolveNaptr } = require('dns');
var toHex = require('colornames');
const internal = require('stream');

if (!Object.entries)
{
    Object.entries = function (obj)
    {
        var ownProps = Object.keys(obj),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array!
        while (i--)
        {
            resArray[i] = [ownProps[i], obj[ownProps[i]]];
        }

        return resArray;
    };
}

exports.admins =
    [
        "124136556578603009", //Timfa,
        "193093226096361472" //Figment
    ];

exports.cmds =
    [
        {
            cmd: "remind",
            params: "Remind a user that attention is required.",
            hidden: false,
            category: "stuff",
            options: [{ type: 6, name: "user", description: "The user to remind!", required: true }, { type: 3, name: "message", description: "Add an optional message", required: false }],
            execute: function (bot, info, args, options)
            {
                var validMention = options[0].value;

                if (validMention)
                {
                    if (validMention)
                    {
                        let channel = info.channelID;

                        bot.data[info.serverId].reminders = bot.data[info.serverId].reminders || {};
                        bot.data[info.serverId].reminders[validMention] = bot.data[info.serverId].reminders[validMention] || [];

                        let exists = false;

                        for (let i = 0; i < bot.data[info.serverId].reminders[validMention].length; i++)
                        {
                            let reminder = bot.data[info.serverId].reminders[validMention][i];

                            if (reminder && reminder.channel == channel)
                            {
                                exists = true;

                                if (reminder.lastPing && (Date.now() - reminder.lastPing) < 7200000)
                                {
                                    info.evt.interaction.reply({ content: "Reminder for " + info.client.users.resolve(validMention).username + " __not__ sent; Another reminder has already been performed recently! (<t:" + Math.round(reminder.lastPing / 1000) + ":R>)" }).then(repl =>
                                    {
                                        setTimeout(() =>
                                        {
                                            info.evt.interaction.deleteReply();
                                        }, 5000);
                                    });
                                    return;
                                }

                                reminder.lastPing = Date.now();
                            }
                        }

                        if (!exists)
                        {
                            bot.data[info.serverId].reminders[validMention].push(
                                {
                                    date: Date.now(),
                                    channel: channel,
                                    lastPing: Date.now()
                                }
                            );
                        }

                        wasUserSearch = true;
                        // ID: validMention[2]<#881236328635858954>
                        console.log("user: " + validMention);

                        let strings = "";
                        for (let i = 0; i < bot.data[info.serverId].reminders[validMention].length; i++)
                        {
                            let ago = "<t:" + Math.round(bot.data[info.serverId].reminders[validMention][i].date / 1000) + ":R>";
                            strings += "- <#" + bot.data[info.serverId].reminders[validMention][i].channel + "> - " + ago + "\n";
                        }

                        let msg = options[1] && options[1].value ? "<@!" + info.userID + ">: \"" + options[1].value + "\"\n" : "";

                        info.client.users.resolve(validMention).send("# Reminder by <@!" + info.userID + ">!\n## Action required in <#" + channel + ">!\n\n" + msg + (strings.length > 1 ? "All reminders:\n" + strings : "") + "\nYou can view your reminders any time by executing `/reminders` in the server.").then(res =>
                        {
                            if (info.evt.interaction)
                            {
                                info.evt.interaction.reply({ content: "Reminder for <#" + channel + "> sent to " + info.client.users.resolve(validMention).username + "!" }).then(repl =>
                                {
                                    setTimeout(() =>
                                    {
                                        info.evt.interaction.deleteReply();
                                    }, 5000);
                                });
                            }
                            else
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "Reminder sent for <#" + channel + ">!",
                                    typing: false
                                }, (err, res) =>
                                {
                                    setTimeout(function ()
                                    {
                                        res.message.delete();
                                    }, 5000);
                                });
                            }
                        });
                    }
                    return;
                }

                if (info.evt.interaction)
                {
                    info.evt.interaction.reply({ content: "Invalid user tag! Correct usage: `/remind @UserName` or `/remind @UserName, #channel`" }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 2000);
                    });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Invalid user tag! Correct usage: `/remind @UserName` or `/remind @UserName, #channel`",
                        typing: false
                    }, function (err, res)
                    {
                        setTimeout(
                            function ()
                            {
                                bot.deleteMessage({
                                    channelID: info.channelID,
                                    messageID: res.id
                                });
                                bot.deleteMessage({
                                    channelID: info.channelID,
                                    messageID: info.evt.d.id
                                });
                            }, 5000
                        );
                    });
                }
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "clearallreminders",
            params: "clear all reminder data.",
            hidden: true,
            category: "stuff",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].reminders = {};

                if (info.evt.interaction)
                {
                    info.evt.interaction.reply({ content: 'Deleted!' });
                    return;
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Deleted!",
                    typing: false
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "reminders",
            params: "Check your pending reminders.",
            hidden: false,
            category: "stuff",
            options: [{ type: 6, name: "username", description: "The user whose reminders you want to check (Default: Yourself)!", required: false }],
            execute: function (bot, info, args, options)
            {
                let idToCheck = options[0] ? options[0].value : info.userID;
                bot.data[info.serverId].reminders = bot.data[info.serverId].reminders || {};
                bot.data[info.serverId].reminders[idToCheck] = bot.data[info.serverId].reminders[idToCheck] || [];
                let strings = "";

                for (let i = 0; i < bot.data[info.serverId].reminders[idToCheck].length; i++)
                {
                    let ago = "<t:" + Math.round(bot.data[info.serverId].reminders[idToCheck][i].date / 1000) + ":R>";
                    strings += "- <#" + bot.data[info.serverId].reminders[idToCheck][i].channel + "> - " + ago + "\n";
                }

                if (info.evt.interaction)
                {
                    info.evt.interaction.reply({ content: strings.length > 0 ? "Reminders for " + info.client.users.resolve(idToCheck).username + "\n" + strings : "No reminders pending!" });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: strings.length > 0 ? strings : "No reminders pending!",
                        typing: false
                    });
                }
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "clearreminders",
            params: "Clear your pending reminders.",
            hidden: false,
            category: "stuff",
            options: [],
            execute: function (bot, info, args, options)
            {
                let idToCheck = info.userID;
                bot.data[info.serverId].reminders = bot.data[info.serverId].reminders || {};
                bot.data[info.serverId].reminders[idToCheck] = [];
                let strings = "";

                for (let i = 0; i < bot.data[info.serverId].reminders[idToCheck].length; i++)
                {
                    let ago = "<t:" + Math.round(bot.data[info.serverId].reminders[idToCheck][i].date / 1000) + ":R>";
                    strings += "<#" + bot.data[info.serverId].reminders[idToCheck][i].channel + "> - " + ago + "\n";
                }

                if (info.evt.interaction)
                {
                    info.evt.interaction.reply({ content: "Reminders for " + info.client.users.resolve(idToCheck).username + " cleared!" });
                }
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "modules",
            params: "Module options.",
            hidden: true,
            category: "admin",
            options: [{ type: 3, name: "module", description: "module name", required: true }, { type: 3, name: "enable", description: "enable/disable", required: true }],
            execute: function (bot, info, args, options)
            {
                bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
                args = options[0].value;

                options[0].value = options[0].value.toLowerCase().trim();
                if (options[1].value.toLowerCase().trim() == "enable")
                {
                    if (bot.data[info.serverId].modules.indexOf(options[0].value) < 0)
                    {
                        if (rpmodules[options[0].value])
                        {
                            bot.data[info.serverId].modules.push(options[0].value);
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Module `" + options[0].value + "` enabled.",
                                typing: false
                            });
                        }
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Module `" + options[0].value + "` not found.",
                                typing: false
                            });
                        }
                    }
                }
                else
                {
                    let index = bot.data[info.serverId].modules.indexOf(options[0].value);
                    if (index >= 0)
                    {
                        bot.data[info.serverId].modules.splice(index, 1);
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Module `" + options[0].value + "` disabled.",
                            typing: false
                        });
                    }
                    else
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Module `" + options[0].value + "` was already disabled.",
                            typing: false
                        });
                    }
                }

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "close",
            params: "Close an RP in this channel, signaling to others that the channel is available again.",
            category: "main",
            options: [{ type: 7, name: "chain", description: "Optional channel name to continue in", required: false }],
            execute: function (bot, info, args, options)
            {
                info.evt.interaction && info.evt.interaction.reply({ content: "**__▓▓▓▓▓▓▓▓▓▓ Scene Closed ▓▓▓▓▓▓▓▓▓▓__**" }).then(repl =>
                {
                    info.evt.interaction.fetchReply().then(reply =>
                    {
                        if (options && options[0] && options[0].value)
                        {
                            bot.sendMessage({
                                to: options[0].value,
                                message: "Continued from " + reply.url,
                                typing: false
                            }, function (url, msg)
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "Continued in " + url,
                                    typing: false
                                });
                            });
                        }
                    });
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "ignorebot",
            params: "Tell RP Fight Bot to ignore a certain bot when keeping track of which character is where.",
            category: "main",
            options: [{ type: 3, name: "bot", description: "The bot to ignore when tracking characters!", required: true }],
            execute: function (bot, info, args, options)
            {
                bot.data[info.serverId].botLocations = bot.data[info.serverId].botLocations || {};
                let name = options[0].value;

                let found = bot.data[info.serverId].botLocations[name.toLowerCase().trim()];

                if (found)
                {
                    delete bot.data[info.serverId].botLocations[name.toLowerCase().trim()];
                }

                bot.data[info.serverId].ignoredBots = bot.data[info.serverId].ignoredBots || {};

                bot.data[info.serverId].ignoredBots[name.trim().toLowerCase()] = true;

                bot.sendMessage({
                    to: info.channelID,
                    message: name + " will no longer be tracked.",
                    typing: false
                });

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "trackbot",
            params: "Tell RP Fight Bot to track a specified bot.",
            category: "main",
            options: [{ type: 3, name: "bot", description: "The bot to ignore when tracking characters!", required: true }],
            execute: function (bot, info, args, options)
            {
                let name = options[0].value;

                let found = bot.data[info.serverId].ignoredBots[name.toLowerCase().trim()];

                if (found)
                {
                    delete bot.data[info.serverId].ignoredBots[name.toLowerCase().trim()];
                    bot.sendMessage({
                        to: info.channelID,
                        message: name + " will be tracked again.",
                        typing: false
                    });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: name + " is already being tracked.",
                        typing: false
                    });
                }

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "ignorechannel",
            params: "Exclude this channel from the character tracker.",
            category: "main",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].ignoredChannels = bot.data[info.serverId].ignoredChannels || {};

                bot.data[info.serverId].ignoredChannels[info.channelID] = true;

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "trackchannel",
            params: "Include this channel in the character tracker.",
            category: "main",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].ignoredChannels = bot.data[info.serverId].ignoredChannels || {};

                if (bot.data[info.serverId].ignoredChannels[info.channelID])
                {
                    delete bot.data[info.serverId].ignoredChannels[info.channelID];

                    bot.sendMessage({
                        to: info.channelID,
                        message: "<#" + info.channelID + "> will be tracked again.",
                        typing: false
                    });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "<#" + info.channelID + "> is already being tracked.",
                        typing: false
                    });
                }

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "find",
            params: "Find the last channel a character has posted in.",
            category: "main",
            options: [{ type: 3, name: "name", description: "The character to find!", required: true }],
            execute: function (bot, info, args, options)
            {
                let strings = [];
                let wasUserSearch = false;

                bot.data[info.serverId].botLocations = bot.data[info.serverId].botLocations || {};

                let query = options[0].value;

                var validMention = query.match(/(<@)!?([0-9]+)(>)/);

                if (validMention)
                {
                    wasUserSearch = true;
                    // ID: validMention[2]
                    for (let [key, value] of Object.entries(bot.data[info.serverId].botLocations))
                    {
                        if (value.user && value.user == validMention[2])
                            strings.push(`${value.name}: ${value.place}`);
                    }
                }
                else
                {
                    for (let [key, value] of Object.entries(bot.data[info.serverId].botLocations))
                    {
                        if (value.name.toLowerCase().includes(query.toLowerCase()))
                            strings.push(`${value.name}: ${value.place}`);
                    }
                }

                strings.sort();

                info.evt.interaction && info.evt.interaction.reply({ content: (wasUserSearch ? "<@" + validMention[2] + ">'s Characters: \n" : "") + strings.join("\n") });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "attack",
            params: "Attack one or more characters.",
            category: "main",
            options: [{ type: 3, name: "attacker", description: "The character to attack with!", required: true }, { type: 3, name: "target", description: "The character(s) to attack! (comma separated)", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var attacker = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var lines = [];

                args = options[1].value.split(",");

                if (attacker)
                {
                    if (attacker.HP <= 0)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: attacker.Name + " is already defeated and cannot attack!",
                            typing: false
                        });
                        return;
                    }

                    var defenders = [];
                    for (var i = 0; i < args.length; i++)
                    {
                        var defender = bot.data[info.serverId].characters[args[i].toUpperCase().trim()];
                        if (!defender)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Defender '" + args[i].trim() + "' not found!",
                                typing: false
                            });
                            return;
                        }

                        if (defender == attacker)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "You cannot attack yourself!",
                                typing: false
                            });
                            return;
                        }

                        if (defender.HP <= 0)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: defender.Name + " is already defeated and cannot be attacked!",
                                typing: false
                            });
                            return;
                        }

                        if (defenders.indexOf(defender) < 0)
                            defenders.push(defender);
                    }

                    if (defenders.length > 1)
                    {
                        if (attacker.CHAOS >= 1)
                            attacker.CHAOS--;
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Attacker '" + attacker.Name + "' does not have enough " + GetChaosName(bot, info, attacker) + " to perform an attack against multiple targets! (required: 1)",
                                typing: false
                            });
                            return;
                        }
                    }

                    lines = PerformAttack(bot, info, attacker, defenders);
                }

                RecursivePost(bot, info, lines);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "startcombat",
            params: "Starts a new combat in this channel and generate a turn order list!",
            category: "main",
            options: [],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                let msg = GenerateInitiativeList(bot, info, [], true);

                bot.sendMessage({
                    to: info.channelID,
                    message: msg,
                    typing: false
                }, function (err, msg)
                {
                    console.log("initiativelist", msg);
                    let id = msg.id;

                    bot.data[info.serverId].combat[info.channelID] =
                    {
                        tracker: id,
                        characters: []
                    };

                    info.evt.interaction && info.evt.interaction.reply({ content: 'Combat has started! You may now roll /initiative!' });
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "initiativelist",
            params: "DEPRECATED! Please use /startcombat instead!",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character(s) to track! (comma separated)", required: true }],
            execute: function (bot, info, args, options)
            {
                args = options[0].value.split(" ");
                CheckIntegrity(bot, info);

                if (!args.join(" ").includes(","))
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "This command requires you to separate the character names with commas!",
                        typing: false
                    });
                    return;
                }

                args = args.join(" ").split(",");

                var lines = [];

                var defenders = [];
                for (var i = 0; i < args.length; i++)
                {
                    var defender = bot.data[info.serverId].characters[args[i].toUpperCase().trim()];
                    if (!defender)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Character '" + args[i].trim() + "' not found!",
                            typing: false
                        });
                        return;
                    }

                    if (defenders.indexOf(defender) < 0)
                    {
                        defenders.push(defender);
                    }
                }

                if (defenders.length > 1)
                {
                    let msg = GenerateInitiativeList(bot, info, defenders, true);

                    bot.sendMessage({
                        to: info.channelID,
                        message: msg,
                        typing: false
                    }, function (err, msg)
                    {
                        console.log("initiativelist", msg);
                        let id = msg.id;
                        let channel = msg.message.channelId;

                        let defenderData = [];
                        for (let i = 0; i < defenders.length; i++)
                        {
                            defenderData.push({
                                Name: defenders[i].Name,
                                HP: defenders[i].HP || 0,
                                lastInitiative: defenders[i].lastInitiative || 0
                            });
                        }

                        for (let i = 0; i < defenders.length; i++)
                        {
                            defenders[i].tracker =
                            {
                                characters: defenderData,
                                msgId: id,
                                channelId: channel,
                                leaving: false
                            };

                            console.log(defenders[i].tracker);
                        }
                    });
                }

                info.evt.interaction && info.evt.interaction.reply({ content: 'Generating self-updating Initiative List!' });
            }
        },
        {
            cmd: "chaosattack",
            params: "Spend all CHAOS for a powerful attack against one or more characters.",
            category: "main",
            options: [{ type: 3, name: "attacker", description: "The character to attack with!", required: true }, { type: 3, name: "target", description: "The character(s) to attack! (comma separated)", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                args = options[1].value.split(",");
                var attacker = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var lines = [];

                if (attacker)
                {
                    if (attacker.HP <= 0)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: attacker.Name + " is already defeated and cannot attack!",
                            typing: false
                        });
                        return;
                    }

                    var defenders = [];
                    for (var i = 0; i < args.length; i++)
                    {
                        var defender = bot.data[info.serverId].characters[args[i].toUpperCase().trim()];
                        if (!defender)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Defender '" + args[i].trim() + "' not found!",
                                typing: false
                            });
                            return;
                        }

                        if (defender == attacker)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "You cannot attack yourself!",
                                typing: false
                            });
                            return;
                        }

                        if (defender.HP <= 0)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: defender.Name + " is already defeated and cannot be attacked!",
                                typing: false
                            });
                            return;
                        }

                        if (defenders.indexOf(defender) < 0)
                            defenders.push(defender);
                    }

                    if (defenders.length > 1)
                    {
                        if (attacker.CHAOS > 1)
                            attacker.CHAOS--;
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Attacker '" + attacker.Name + "' does not have enough " + GetChaosName(bot, info, attacker) + " to perform an attack against multiple targets! (required: 2 or more, one point is consumed _before_ the " + GetChaosName(bot, info, attacker) + " attack is executed)",
                                typing: false
                            });
                            return;
                        }
                    }

                    lines = PerformAttack(bot, info, attacker, defenders, true);
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Attacker '" + options[0].value.trim() + "' not found!",
                        typing: false
                    });
                    return;
                }

                RecursivePost(bot, info, lines);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "heal",
            params: "Spend 3 CHAOS to heal one or more characters. Mystic healers will first spend Manaward instead.",
            category: "main",
            options: [{ type: 3, name: "healer", description: "The character to heal with!", required: true }, { type: 3, name: "target", description: "The character(s) to heal! (comma separated)", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                args = options[1].value.split(",");

                var attacker = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var healername = options[0].value;
                var lines = [];

                if (attacker)
                {
                    if (attacker.HP <= 0)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: attacker.Name + " is already defeated and cannot heal!",
                            typing: false
                        });
                        return;
                    }

                    var cost = 3;

                    if (attacker.CLASS == "Mystic")
                    {
                        if (attacker.RESOURCE > 0)
                        {
                            if (attacker.RESOURCE >= cost)
                            {
                                attacker.RESOURCE -= cost;
                                cost = 0;
                            }
                            else
                            {
                                cost -= attacker.RESOURCE;
                                attacker.RESOURCE = 0;
                            }
                        }
                    }

                    if (attacker.CHAOS < cost)
                    {
                        ok = false;

                        if (!ok)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "You do not have enough " + GetChaosName(bot, info, attacker) + (attacker.CLASS == "Mystic" ? " or Manaward" : "") + " to heal! You need at least 3 " + GetChaosName(bot, info, attacker) + ".",
                                typing: false
                            });
                            return;
                        }
                    }

                    attacker.CHAOS -= cost;

                    var defenders = [];
                    for (var i = 0; i < args.length; i++)
                    {
                        var defender = bot.data[info.serverId].characters[args[i].toUpperCase().trim()];
                        if (!defender)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Target '" + args[i].trim() + "' not found!",
                                typing: false
                            });
                            return;
                        }

                        if (defenders.indexOf(defender) < 0)
                            defenders.push(defender);
                    }

                    if (defenders.length == 0)
                        defenders.push(attacker);

                    lines = PerformHeal(bot, info, attacker, defenders);
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Healer '" + healername.trim() + "' not found!",
                        typing: false
                    });
                    return;
                }

                RecursivePost(bot, info, lines);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "boost",
            params: "Boost another character with an Oracle, increasing the success rate of attacks.",
            category: "main",
            options: [{ type: 3, name: "oracle", description: "The character to boost with!", required: true }, { type: 3, name: "target", description: "The character(s) to boost! (comma separated)", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                args = options[1].value.split(",");

                var attacker = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var healername = options[0].value;
                var lines = [];

                if (attacker)
                {
                    if (attacker.HP <= 0)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: attacker.Name + " is already defeated and cannot boost!",
                            typing: false
                        });
                        return;
                    }

                    if (attacker.CLASS != "Oracle")
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You are not an Oracle!",
                            typing: false
                        });
                        return;
                    }

                    var defenders = [];
                    for (var i = 0; i < args.length; i++)
                    {
                        var defender = bot.data[info.serverId].characters[args[i].toUpperCase().trim()];
                        if (!defender)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Target '" + args[i].trim() + "' not found!",
                                typing: false
                            });
                            return;
                        }

                        if (defender.HP <= 0)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: defender.Name + " is already defeated and cannot be boosted!",
                                typing: false
                            });
                            return;
                        }

                        if (defenders.indexOf(defender) < 0)
                            defenders.push(defender);
                    }

                    if (defenders.length == 0)
                        defenders.push(attacker);

                    lines = PerformBoost(bot, info, attacker, defenders);
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Oracle '" + healername.trim() + "' not found!",
                        typing: false
                    });
                    return;
                }

                RecursivePost(bot, info, lines);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "buff",
            params: "Buff a character using a Savant. This will increase attack potency for a few turns.",
            category: "main",
            options: [{ type: 3, name: "savant", description: "The character to buff with!", required: true }, { type: 3, name: "target", description: "The character to buff!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var healer = options[0].value;
                var healee = options[1].value;

                var attacker = bot.data[info.serverId].characters[healer.toUpperCase().trim()];
                var form = "Savant '" + healer.trim() + "' not found";

                if (attacker)
                {
                    if (attacker.HP <= 0)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: attacker.Name + " is already defeated and cannot buff!",
                            typing: false
                        });
                        return;
                    }

                    var defender = bot.data[info.serverId].characters[healee.toUpperCase().trim()];
                    var form = "Target '" + healee.trim() + "' not found";

                    if (defender)
                    {
                        if (attacker.CLASS != "Savant")
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "You are not a Savant!",
                                typing: false
                            });
                            return;
                        }

                        if (attacker.RESOURCE < 3)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Your Ingenuity buff is not yet ready.",
                                typing: false
                            });
                            return;
                        }

                        if (defender.HP <= 0)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: defender.Name + " is already defeated and cannot be buffed!",
                                typing: false
                            });
                            return;
                        }

                        attacker.RESOURCE = 0;

                        attacker.prevAttackDidFail = false;
                        let buffDone = Math.ceil(Math.max(1, GetStatMod(GetStatNum(attacker, "INT"))));

                        let extraLines = [];

                        bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
                        for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
                        {
                            let module = rpmodules[bot.data[info.serverId].modules[i]];
                            bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
                            let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};
                            let response = null;

                            response = module.onBuff && module.onBuff(mdata, bot, attacker, info, defender, buffDone);
                            if (response.strength)
                                buffDone = response.strength;

                            if (response.extraText)
                                extraLines.push(response.extraText);

                            response = module.onBuffed && module.onBuffed(mdata, bot, defender, info, attacker, buffDone);
                            if (response.strength)
                                buffDone = response.strength;

                            if (response.extraText)
                                extraLines.push(response.extraText);

                            bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
                        }

                        var attacksstring = buffDone == 1 ? "attack" : buffDone + " attacks";

                        var adds = defender.Name[defender.Name.length - 1].toLowerCase() != "s";
                        form = attacker.Name + " buffs " + (defender.Name == attacker.Name ? " the next " + attacksstring : defender.Name + (adds ? "'s" : "'") + " next " + attacksstring) + "!";

                        if (extraLines.length > 0)
                            form += "\n" + extraLines.join("\n");

                        defender.BUFF = buffDone;
                    }
                }

                attacker = attacker || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, attacker.Name, attacker.Avatar, form, null, true, "Buff!");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "ward",
            params: "Ward a character using a Mystic to protect it from damage using 3 Manaward points.",
            category: "main",
            options: [{ type: 3, name: "mystic", description: "The character to ward with!", required: true }, { type: 3, name: "target", description: "The character(s) to ward! (comma separated)", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                args = options[1].value.split(",");

                var attacker = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var healername = options[0].value;
                var lines = [];

                if (attacker)
                {
                    if (attacker.HP <= 0)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: attacker.Name + " is already defeated and cannot ward!",
                            typing: false
                        });
                        return;
                    }

                    if (attacker.CLASS != "Mystic")
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You are not a Mystic!",
                            typing: false
                        });
                        return;
                    }

                    if (attacker.RESOURCE < 3)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You do not have enough Manaward! You require 3 Manaward to activate a Ward.",
                            typing: false
                        });
                        return;
                    }

                    attacker.RESOURCE -= 3;

                    var defenders = [];
                    for (var i = 0; i < args.length; i++)
                    {
                        var defender = bot.data[info.serverId].characters[args[i].toUpperCase().trim()];
                        if (!defender)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Target '" + args[i].trim() + "' not found!",
                                typing: false
                            });
                            return;
                        }

                        if (defender.HP <= 0)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: defender.Name + " is already defeated and cannot be warded!",
                                typing: false
                            });
                            return;
                        }

                        if (defenders.indexOf(defender) < 0)
                            defenders.push(defender);
                    }

                    if (defenders.length == 0)
                        defenders.push(attacker);

                    lines = PerformWard(bot, info, attacker, defenders);
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Mystic '" + healername.trim() + "' not found!",
                        typing: false
                    });
                    return;
                }

                RecursivePost(bot, info, lines);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "check",
            params: "Perform a check for a character with a specific stat.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to make a check with!", required: true }, { type: 3, name: "stat", description: "The stat to make a check with!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var attacker = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (attacker)
                {
                    options[1].value = options[1].value.trim().toUpperCase();
                    form = "Stat '" + options[1].value + "' is invalid!";

                    if (options[1].value.toUpperCase() == "ATTACK")
                        options[1].value = "RANK";

                    if (options[1].value == "DEX" || options[1].value == "CON" || options[1].value == "STR" || options[1].value == "INT" || options[1].value == "CHA" || options[1].value == "RANK")
                    {
                        attackRoll = CheckStat(attacker, options[1].value);

                        let extraLines = [];
                        bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
                        for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
                        {
                            let module = rpmodules[bot.data[info.serverId].modules[i]];
                            bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
                            let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

                            let response = module.onCheck && module.onCheck(mdata, bot, attacker, info, options[1].value, attackRoll);
                            if (response.roll)
                                attackRoll = response.roll;

                            if (response.stat)
                                options[1].value = response.stat;

                            if (response.extraText)
                                extraLines.push(response.extraText);

                            bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
                        }

                        form = attacker.Name + " performs " + (options[1].value == "INT" || options[1].value == "RANK" ? "an " : "a ") + options[1].value.replace("RANK", "attack") + (options[1].value == "RANK" ? ", dealing" : " Check, with a result of") + " " + Math.round(attackRoll) + (options[1].value == "RANK" ? " points of damage." : ".");
                        if (extraLines.length > 0)
                            form += "\n" + extraLines.join("\n");
                    }

                    if (options[1].value == "TOHIT")
                    {
                        var boost = 2;

                        if (attacker.BOOST)
                        {
                            boost += attacker.BOOST;

                            if (attacker.BOOST <= 0)
                                delete attacker.BOOST;
                        }

                        attackRoll = Math.ceil(RollMod(GetToHitMod(attacker), boost));

                        let extraLines = [];
                        bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
                        for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
                        {
                            let module = rpmodules[bot.data[info.serverId].modules[i]];
                            bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
                            let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

                            let response = module.onCheck && module.onCheck(mdata, bot, attacker, info, options[1].value, attackRoll);
                            if (response.roll)
                                attackRoll = response.roll;

                            if (response.stat)
                                options[1].value = response.stat;

                            if (response.extraText)
                                extraLines.push(response.extraText);

                            bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
                        }

                        form = attacker.Name + " performs a To-Hit check , with a result of: " + Math.round(attackRoll) + ".";
                        if (extraLines.length > 0)
                            form += "\n" + extraLines.join("\n");
                    }
                }

                attacker = attacker || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, attacker.Name, attacker.Avatar, form, null, true, "Ability Check:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "contest",
            params: "Perform a contest for a character with a specific stat against another character.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to make a check with!", required: true }, { type: 3, name: "stat", description: "The stat to make a check with!", required: true }, { type: 3, name: "target", description: "The character to make a contest against!", required: true }, { type: 3, name: "statdefense", description: "The stat to make a check against!", required: false }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var attacker = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var defender = bot.data[info.serverId].characters[options[2].value.toUpperCase().trim()];
                var form = "Character not found";

                if (attacker && defender)
                {
                    options[1].value = options[1].value.trim().toUpperCase();
                    form = "Stat '" + options[1].value + "' is invalid!";

                    let defendStat = options[1].value;

                    try
                    {
                        defendStat = options[3].value.trim().toUpperCase();
                    }
                    catch (e)
                    {
                        console.log("On contest: could not read DefendStat from options.");
                    }

                    if (options[1].value == "DEX" || options[1].value == "CON" || options[1].value == "STR" || options[1].value == "INT" || options[1].value == "CHA")
                    {
                        let attackRoll = CheckStat(attacker, options[1].value);

                        if (defendStat == "DEX" || defendStat == "CON" || defendStat == "STR" || defendStat == "INT" || defendStat == "CHA")
                        {
                            let defendRoll = GetStatNum(defender, defendStat);

                            form = attacker.Name + " performs " + (options[1].value == "INT" ? "an " : "a ") + options[1].value + " contest" + (defendStat != options[1].value ? " against " + defendStat : "") + ", with a result of **" + Math.round(attackRoll) + " vs " + defendRoll + "**.";
                            let difference = Math.round(attackRoll) - defendRoll;
                            let success = difference >= 0 ? "Success" : "Failure";

                            switch (Math.abs(difference))
                            {
                                case 0: form += "\n## Narrowly Succeeded"; break;
                                case 1: form += "\n## Minor " + success; break;
                                case 2: form += "\n## Small " + success; break;
                                case 3: form += "\n## Moderate " + success; break;
                                case 4: form += "\n## Decent " + success; break;
                                case 5: form += "\n## **Significant " + success + "!**"; break;
                                case 6: form += "\n## **Substantial " + success + "!**"; break;
                                case 7: form += "\n## **Impressive " + success + "!**"; break;
                                case 8: form += "\n## **Major " + success + "!**"; break;
                                case 9: form += "\n## **Critical " + success + "!**"; break;
                                default: form += "\n## __Overwhelming " + success + "!__"; break;
                            }
                        }
                        else
                        {
                            form = "Stat '" + options[1].value + "' is invalid!";
                        }
                    }
                }

                attacker = attacker || { Name: "RP Fight Bot" };
                defender = defender || { Name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, attacker.Name, attacker.Avatar, form, null, true, "Contest against " + defender.Name + ":");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "initiative",
            params: "Roll Initiative with a character in order to enter combat.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to make an initiative check with!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                charName = options[0].value;

                var attacker = bot.data[info.serverId].characters[charName.toUpperCase().trim()];
                var form = "Character '" + charName.trim() + "' not found";

                if (attacker)
                {
                    attackRoll = Math.round(CheckDEX(attacker));
                    attacker.CHAOS = 0;
                    attacker.RESOURCE = 0;

                    if (attacker.BUFF)
                        delete attacker.BUFF;

                    if (attacker.WARD)
                        delete attacker.WARD;

                    if (attacker.BOOST)
                        delete attacker.BOOST;

                    bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];

                    let extraLines = [];

                    for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
                    {
                        let module = rpmodules[bot.data[info.serverId].modules[i]];

                        if (!module)
                            continue;

                        bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
                        let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

                        let response = module && module && module.onInitiative && module.onInitiative(mdata, bot, attacker, info, attackRoll);

                        if (response.roll)
                            attackRoll = response.roll;

                        if (response.extraText)
                            extraLines.push(response.extraText);

                        bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
                    }


                    form = attacker.Name + " has an Initiative of " + attackRoll + "!";
                    if (extraLines.length > 0)
                        form += "\n" + extraLines.join("\n");

                    if (attacker.tracker)
                    {
                        if (attacker.tracker.channelId != info.channelID)
                        {
                            for (let i = 0; i < bot.data[info.serverId].combat[attacker.tracker.channelId].characters.length; i++)
                            {
                                if (bot.data[info.serverId].combat[attacker.tracker.channelId].characters[i].Name == attacker.Name)
                                {
                                    bot.data[info.serverId].combat[attacker.tracker.channelId].characters[i].hasLeft = true;
                                    bot.data[info.serverId].combat[attacker.tracker.channelId].characters[i].last = attacker.lastInitiative;
                                }
                            }

                            UpdateInitiativeTracker(bot, info, { modern: true, channelId: attacker.tracker.channelId });

                            delete attacker.tracker;
                        }
                    }

                    attacker.lastInitiative = attackRoll;

                    if (!attacker.tracker || attacker.tracker.modern)
                    {
                        if (bot.data[info.serverId].combat[info.channelID])
                        {
                            let found = false;

                            for (let i = 0; i < bot.data[info.serverId].combat[info.channelID].characters.length; i++)
                            {
                                if (bot.data[info.serverId].combat[info.channelID].characters[i].Name == attacker.Name)
                                {
                                    found = true;
                                    bot.data[info.serverId].combat[info.channelID].characters[i].hasLeft = false;
                                }
                            }

                            if (!found)
                                bot.data[info.serverId].combat[info.channelID].characters.push({ Name: attacker.Name });

                            attacker.tracker =
                            {
                                modern: true,
                                channelId: info.channelID,
                                leaving: false
                            };
                        }
                    }

                    if (attacker.tracker)
                        UpdateInitiativeTracker(bot, info, attacker.tracker);

                    attacker = attacker || { name: "RP Fight Bot" };
                    bot.PostWebhook(bot, info.serverId, info.channelID, attacker.Name, attacker.Avatar, form, null, true, "Initiative:");

                    info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 2000);
                    });
                }
                else
                {
                    info.evt.interaction && info.evt.interaction.reply({ content: form }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 2000);
                    });
                }
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "flavorchaos",
            params: "Change the name of a character's CHAOS.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to reflavor!", required: true }, { type: 3, name: "name", description: "The new CHAOS name!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                charName = options[0].value;
                chaosName = options[1].value;

                var attacker = bot.data[info.serverId].characters[charName.toUpperCase().trim()];
                var form = "Character '" + charName.trim() + "' not found";

                if (attacker)
                {
                    attacker.ChaosName = chaosName.toUpperCase().trim();
                    form = "CHAOS label for " + attacker.Name + " set to " + attacker.ChaosName;
                }

                attacker = attacker || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, attacker.Name, attacker.Avatar, form, null, true, "CHAOS Reflavored:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "set",
            params: "Alter a character's stat.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to edit!", required: true }, { type: 3, name: "stat", description: "The stat to adjust!", required: true }, { type: 3, name: "value", description: "The new value!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    options[1].value = options[1].value.trim().toUpperCase();
                    if (options[1].value == GetClassResourceName(character.CLASS).toUpperCase()) options[1].value = "RESOURCE";

                    form = "Stat name '" + options[1].value + "' is invalid!";

                    if (options[1].value == "AVATAR" || options[1].value == "RESOURCE" || options[1].value == "DEX" || options[1].value == "CON" || options[1].value == "STR" || options[1].value == "INT" || options[1].value == "CHA" || options[1].value == "HP" || options[1].value == "RANK" || options[1].value == "CLASS" || options[1].value == "CHAOS")
                    {
                        if (options[1].value == "RANK")
                        {
                            var form = "Ranks no longer need to be manually assigned!";
                        }
                        else if (options[1].value == "CLASS")
                        {
                            options[2].value = CapitalizeFirstLetter(options[2].value.trim().toLowerCase());

                            var form = "Class '" + options[2].value + "' invalid! Valid classes are 'Fighter', 'Bruiser', 'Dasher', 'Mystic', 'Oracle' or 'Savant'!";

                            if (options[2].value == "Fighter" || options[2].value == "Bruiser" || options[2].value == "Dasher" || options[2].value == "Mystic" || options[2].value == "Savant" || options[2].value == "Oracle")
                            {
                                character.CLASS = options[2].value;

                                var form = GetCharacterDetailForm(bot, info, character);
                            }
                        }
                        else if (options[1].value == "AVATAR")
                        {
                            character.Avatar = options[2].value.trim();
                            var form = "Set!";
                        }
                        else
                        {
                            if (Number.isNaN(Number.parseInt(options[2].value)))
                            {
                                form = "'" + options[2].value.trim() + "' is not a number!";
                            }
                            else
                            {
                                if (Number.parseInt(options[2].value) < (options[1].value == GetChaosName(bot, info, character) || options[1].value == "RESOURCE" ? 0 : 1))
                                {
                                    form = "Stats cannot be lower than 1!";
                                }
                                else
                                {
                                    character[options[1].value] = Number.parseInt(options[2].value);

                                    character.HP = Math.min(character.HP, GetMaxHP(character));
                                    character.CHAOS = Math.min(character.CHAOS, GetMaxCHAOS(character));

                                    CapResource(character);

                                    var form = GetCharacterDetailForm(bot, info, character);
                                }
                            }
                        }
                    }
                }

                character.HP = Math.min(GetMaxHP(character), character.HP);
                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, null, true, options[1].value.trim().toUpperCase() + " Adjustment:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "powerbonus",
            params: "Set the Power Bonus for a character, increasing all stats for each point.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to edit!", required: true }, { type: 4, name: "points", description: "The new power value!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    if (Number.isNaN(Number.parseInt(options[1].value)))
                    {
                        form = "'" + options[1].value.trim() + "' is not a number!";
                    }
                    else
                    {
                        if (Number.parseInt(options[1].value) < 0)
                        {
                            form = "Power Bonus cannot be lower than 0!";
                        }
                        else
                        {
                            character.Emeralds = Number.parseInt(options[1].value);

                            var form = GetCharacterDetailForm(bot, info, character);
                        }
                    }
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Power Bonus:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "colour",
            params: "Change the embed colour of a character.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to edit!", required: true }, { type: 3, name: "colour", description: "The new HEX colour!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    let bad = false;
                    options[1].value = options[1].value.toUpperCase().replace("#", "").replace("0X", "").trim();

                    if (options[1].value.match("[^A-Fa-f0-9]+") || options[1].value.length != 6)
                    {
                        let asHex = toHex(options[1].value);
                        if (asHex)
                        {
                            options[1].value = asHex.toUpperCase().replace("#", "").replace("0X", "").trim();
                        }
                        else
                        {
                            bad = true;
                        }
                    }

                    if (bad)
                    {
                        form = "'" + options[1].value.trim() + "' is not a valid colour!";
                    }
                    else
                    {
                        character.COLOR = eval("0x" + options[1].value);
                        form = "New colour: `0x" + options[1].value + "`!";
                    }
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Colour for " + character.Name + " changed");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "avatar",
            params: "Change the avatar of a character.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to edit!", required: true }, { type: 3, name: "url", description: "The new avatar URL!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    character.Avatar = options[1].value;
                    form = "New URL: `" + options[1].value + "`!";
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Avatar for " + character.Name + " changed");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "enablesuper",
            params: "Supercharge your character!",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to power up!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    if (!character.SUPER)
                    {
                        form = "►►" + character.Name.toUpperCase() + " IS GOING SUPER◄◄";
                        character.SUPER = true;
                        character.HP = GetMaxHP(character);
                    }
                    else
                    {
                        form = character.Name + " is already super!";
                    }
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "disablesuper",
            params: "Restore your character to its normal power level.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to power down!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    form = character.Name + " is no longer super.";
                    character.SUPER = false;

                    character.hp = Math.min(character.HP, GetMaxHP(character));
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "rename",
            params: "Give a character a new name.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to rename!", required: true }, { type: 3, name: "name", description: "The new name!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    character.Name = options[1].value.trim();
                    bot.data[info.serverId].characters[options[1].value.toUpperCase().trim()] = character;

                    if (options[0].value.trim().toLowerCase() != options[1].value.trim().toLowerCase())
                        delete bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];

                    var form = GetCharacterDetailForm(bot, info, character);
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Rename:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "add",
            params: "Alter a character's stat.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to edit!", required: true }, { type: 3, name: "stat", description: "The stat to adjust!", required: true }, { type: 4, name: "number", description: "The amount to adjust it by!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    options[1].value = options[1].value.trim().toUpperCase();

                    form = "Stat name '" + options[1].value + "' is invalid!";

                    if (options[1].value == "DEX" || options[1].value == "CON" || options[1].value == "STR" || options[1].value == "INT" || options[1].value == "CHA" || options[1].value == "HP" || options[1].value == GetChaosName(bot, info, character))
                    {

                        if (Number.isNaN(Number.parseInt(options[2].value)))
                        {
                            form = "'" + options[2].value.trim() + "' is not a number!";
                        }
                        else
                        {
                            if (character[options[1].value] + Number.parseInt(options[2].value) < (options[1].value == GetChaosName(bot, info, character) ? 0 : 1))
                            {
                                form = "Stats cannot be lower than 1!";
                            }
                            else
                            {
                                character[options[1].value] += Number.parseInt(options[2].value);

                                character.HP = Math.min(character.HP, GetMaxHP(character));
                                character.CHAOS = Math.min(character.CHAOS, GetMaxCHAOS(character));

                                var form = GetCharacterDetailForm(bot, info, character);
                            }
                        }
                    }
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, options[1].value.trim().toUpperCase() + " Adjustment:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "subtract",
            params: "Alter a character's stat.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to edit!", required: true }, { type: 3, name: "stat", description: "The stat to adjust!", required: true }, { type: 4, name: "number", description: "The amount to adjust it by!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character '" + options[0].value.trim() + "' not found";

                if (character)
                {
                    options[1].value = options[1].value.trim().toUpperCase();

                    form = "Stat name '" + options[1].value + "' is invalid!";

                    if (options[1].value == "DEX" || options[1].value == "CON" || options[1].value == "STR" || options[1].value == "INT" || options[1].value == "CHA" || options[1].value == "HP" || options[1].value == GetChaosName(bot, info, character))
                    {

                        if (Number.isNaN(Number.parseInt(options[2].value)))
                        {
                            form = "'" + options[2].value.trim() + "' is not a number!";
                        }
                        else
                        {
                            if (character[options[1].value] - Number.parseInt(options[2].value) < (options[1].value == GetChaosName(bot, info, character) ? 0 : 1))
                            {
                                form = "Stats cannot be lower than 1!";
                            }
                            else
                            {
                                character[options[1].value] -= Number.parseInt(options[2].value);

                                character.HP = Math.min(character.HP, GetMaxHP(character));
                                character.CHAOS = Math.min(character.CHAOS, GetMaxCHAOS(character));

                                var form = GetCharacterDetailForm(bot, info, character);
                            }
                        }
                    }
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, options[1].value.trim().toUpperCase() + " Adjustment:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "register",
            params: "Register a new character.",
            category: "main",
            options: [{ type: 3, name: "name", description: "The new character's name!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "That character already exists in this server!";

                if (!character)
                {
                    character = { Name: options[0].value.trim(), HP: 1, DEX: 1, CON: 1, STR: 1, INT: 1, CHA: 1, CHAOS: 0, RANK: "C", CLASS: "None", Emeralds: 0, RESOURCE: 0 };

                    bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()] = character;
                    var form = "Character Registered!\nNAME: " + character.Name;
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Character Registration: " + character.Name);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "qregister",
            params: "Quickly register a new character with preset stats and class.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The new character's name!", required: true }, { type: 4, name: "str", description: "STR value!", required: true }, { type: 4, name: "dex", description: "DEX value!", required: true }, { type: 4, name: "con", description: "CON value!", required: true }, { type: 4, name: "int", description: "INT value!", required: true }, { type: 4, name: "cha", description: "CHA value!", required: true }, { type: 3, name: "class", description: "the character's class!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "That character already exists in this server!";

                if (!character)
                {

                    character = { Name: options[0].value.trim(), HP: 1, DEX: parseInt(options[2].value), CON: parseInt(options[3].value), STR: parseInt(options[1].value), INT: parseInt(options[4].value), CHA: parseInt(options[5].value), CHAOS: 0, RANK: "C", CLASS: CapitalizeFirstLetter(options[6].value.trim().toLowerCase()), Emeralds: 0, RESOURCE: 0 };

                    bot.data[info.serverId].characters[options[0].value.trim().toUpperCase()] = character;
                    character.HP = GetMaxHP(character);
                    var form = "Character Quick-Registered!\n" + GetCharacterDetailForm(bot, info, character);
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Character Quick-Registration: " + character.Name);

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "bregister",
            params: "Quickly register a series of character with preset stats and class.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The new character's base name!", required: true }, { type: 4, name: "amount", description: "Amount of characters to create", required: true }, { type: 4, name: "str", description: "STR value!", required: true }, { type: 4, name: "dex", description: "DEX value!", required: true }, { type: 4, name: "con", description: "CON value!", required: true }, { type: 4, name: "int", description: "INT value!", required: true }, { type: 4, name: "cha", description: "CHA value!", required: true }, { type: 3, name: "class", description: "the character's class!", required: true }, { type: 3, name: "avatar", description: "the character's avatar!", required: true }],
            execute: function (bot, info, args, options)
            {
                if (options[1].value > 99)
                {
                    info.evt.interaction && info.evt.interaction.reply({ content: 'No more than 99 allowed.' }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 2000);
                    });
                    return;
                }

                CheckIntegrity(bot, info);
                for (let i = 0; i < options[1].value; i++)
                {
                    let numberStr = (i <= 8 ? "0" + (i - -1) : i - -1);
                    let nameInput = options[0].value + numberStr;
                    var character = bot.data[info.serverId].characters[nameInput.toUpperCase().trim()];
                    var form = nameInput + " already exists in this server!";

                    if (!character)
                    {
                        character = { Name: nameInput.trim(), HP: 1, DEX: parseInt(options[3].value), CON: parseInt(options[4].value), STR: parseInt(options[2].value), INT: parseInt(options[5].value), CHA: parseInt(options[6].value), CHAOS: 0, RANK: "C", CLASS: CapitalizeFirstLetter(options[7].value.trim().toLowerCase()), Emeralds: 0, RESOURCE: 0, Avatar: options[8].value };

                        bot.data[info.serverId].characters[nameInput.trim().toUpperCase()] = character;
                        character.HP = GetMaxHP(character);
                        var form = "Character Quick-Registered!\n" + GetCharacterDetailForm(bot, info, character);
                    }

                    character = character || { name: "RP Fight Bot" };
                    bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Character Quick-Registration: " + character.Name);
                }

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "delete",
            params: "Permanently delete a character.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to delete!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "That character doesn't exist in this server!";

                if (character)
                {
                    delete bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];

                    bot.sendMessage({
                        to: info.channelID,
                        message: options[0].value.toUpperCase().trim() + " exists no more.",
                        typing: false
                    }, (err, res) =>
                    {
                        /*setTimeout(function() 
                        { 
                            bot.deleteMessage({
                                channelID: info.channelID,
                                messageID: res.id
                            });
                        }, 5000);*/
                    });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: options[0].value.toUpperCase().trim() + " already doesn't exist.",
                        typing: false
                    }, (err, res) =>
                    {
                        /*setTimeout(function() 
                        { 
                            bot.deleteMessage({
                                channelID: info.channelID,
                                messageID: res.id
                            });
                        }, 5000);*/
                    });
                }

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "leavecombat",
            params: "Remove a character from combat.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to remove from combat!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character not found!";

                if (character)
                {
                    character.tracker.leaving = true;

                    if (character.tracker && character.tracker.modern)
                    {
                        for (let i = 0; i < bot.data[info.serverId].combat[info.channelID].characters.length; i++)
                        {
                            if (bot.data[info.serverId].combat[info.channelID].characters[i].Name == character.Name)
                            {
                                bot.data[info.serverId].combat[info.channelID].characters[i].hasLeft = true;

                                bot.data[info.serverId].combat[attacker.tracker.channelId].characters[i].last = character.lastInitiative;
                            }
                        }
                    }

                    UpdateInitiativeTracker(bot, info, character.tracker);
                    character.tracker = null;
                    bot.sendMessage({
                        to: info.channelID,
                        message: character.Name + " has left the fight.",
                        typing: false
                    });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: form,
                        typing: false
                    }, (err, res) =>
                    {
                        setTimeout(function ()
                        {
                            bot.deleteMessage({
                                channelID: info.channelID,
                                messageID: res.id
                            });
                        }, 60000);
                    });
                }

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "details",
            params: "Check a character's details.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to obtain details of!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character not found!";

                if (character)
                {
                    let extraLines = [];

                    bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
                    for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
                    {
                        let module = rpmodules[bot.data[info.serverId].modules[i]];

                        if (!module)
                            continue;

                        bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
                        let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

                        let response = module.onDetails && module.onDetails(mdata, bot, character, info);

                        if (response.extraText)
                            extraLines.push(response.extraText);

                        bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
                    }

                    var form = GetCharacterDetailForm(bot, info, character);
                    if (extraLines.length > 0)
                        form += "\n" + extraLines.join("\n");

                    bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, character.Name);

                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: form,
                        typing: false
                    }, (err, res) =>
                    {
                        /*setTimeout(function() 
                        { 
                            bot.deleteMessage({
                                channelID: info.channelID,
                                messageID: res.id
                            });
                        }, 60000);*/
                    });
                }

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "speak",
            params: "Post a text message as your character.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to post as!", required: true }, { type: 3, name: "message", description: "The message to post!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var name = options[0].value;
                var content = options[1].value.replaceAll("\\n", "\n");

                var character = bot.data[info.serverId].characters[name.toUpperCase().trim()];
                var form = "Character not found!";

                if (character)
                {
                    bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, content);

                    info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 2000);
                    });
                }
                else
                {
                    info.evt.interaction.reply({ content: form }).then(repl =>
                    {
                        setTimeout(() =>
                        {
                            info.evt.interaction.deleteReply();
                        }, 5000);
                    });
                }
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "reset",
            params: "Reset a character's stats to 0.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to reset! This sets all stats to 0!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character not found!";

                if (character)
                {
                    character.CLASS = "None";
                    character.HP = 1;
                    character.DEX = 1;
                    character.CON = 1;
                    character.STR = 1;
                    character.CHA = 1;
                    character.INT = 1;
                    character.RESOURCE = 0;
                    var form = GetCharacterDetailForm(bot, info, character);
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Character Reset:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "resethealth",
            params: "Fully heal a character.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to restore full health to!", required: true }],
            execute: function (bot, info, args, options)
            {
                info.evt.interaction && info.evt.interaction.reply({ content: 'Resetting ' + options[0].value.trim() }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });

                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character not found!";

                if (character)
                {
                    bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()].HP = GetMaxHP(character);

                    UpdateInitiativeTracker(bot, info, character.tracker);

                    form = HealthBar(character.HP, GetMaxHP(character));
                }

                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Health Reset:");
                character = character || { name: "RP Fight Bot" };
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "health",
            params: "Check a character's current health.",
            category: "main",
            options: [{ type: 3, name: "character", description: "The character to check the health of!", required: true }],
            execute: function (bot, info, args, options)
            {
                CheckIntegrity(bot, info);

                var character = bot.data[info.serverId].characters[options[0].value.toUpperCase().trim()];
                var form = "Character not found!";

                if (character)
                {
                    var form = HealthBar(character.HP, GetMaxHP(character));
                }

                character = character || { name: "RP Fight Bot" };
                bot.PostWebhook(bot, info.serverId, info.channelID, character.Name, character.Avatar, form, null, true, "Character Health:");

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "characters",
            params: "Conjure a text file with the names of all characters currently registered.",
            category: "main",
            execute: function (bot, info, args)
            {
                var names = [];
                for (var prop in bot.data[info.serverId].characters)
                {
                    if (bot.data[info.serverId].characters.hasOwnProperty(prop))
                    {
                        names.push(prop);
                    }
                }

                fs.writeFile("./characters.txt", names.join("\n"), function (err)
                {
                    if (err)
                    {
                        console.log(err);
                    }

                    bot.uploadFile({
                        to: info.channelID,
                        file: "characters.txt"
                    }, (err, res) =>
                    {
                        /*setTimeout(function() 
                        { 
                            bot.deleteMessage({
                                channelID: info.channelID,
                                messageID: res.id
                            });
                        }, 60000);*/
                    });

                    fs.unlinkSync("./characters.txt");
                });

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "invitelink",
            params: "Invite this bot to another server",
            category: "main",
            execute: function (bot, info, args)
            {
                info.evt.interaction && info.evt.interaction.reply({ content: 'https://discordapp.com/oauth2/authorize?&client_id=867010144067125259&scope=bot&permissions=339008' });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "update",
            params: "Update the bot to the newest version",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                var exec = require('child_process').exec;

                bot.data.update = info.channelID;

                bot.sendMessage({
                    to: info.channelID,
                    message: "Fetching changes...",
                    typing: false
                }, function ()
                {
                    exec('git fetch --all', function (err, stdout, stderr)
                    {
                        exec('git log --oneline master..origin/master', function (err, stdout, stderr)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: (stdout == "" ? "No changes, " : "Changes: ```" + stdout + "``` Updating and ") + "reloading!",
                                typing: false
                            }, function ()
                            {
                                bot.suicide();
                            });
                        });
                    });
                });

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "version",
            params: "Check the current bot version",
            category: "main",
            execute: function (bot, info, args)
            {
                var exec = require('child_process').exec;
                exec('git log --oneline -n 1 HEAD', function (err, stdout, stderr)
                {
                    console.log(stdout);
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Current version: ```" + stdout + "```",
                        typing: false
                    });
                });

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' }).then(repl =>
                {
                    setTimeout(() =>
                    {
                        info.evt.interaction.deleteReply();
                    }, 2000);
                });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        },
        {
            cmd: "dumpmem",
            params: "Dump RP Fight Bot memory to a JSON file.",
            category: "Main",
            execute: function (bot, info, args)
            {
                var url = "botData.json";

                info.evt.interaction && info.evt.interaction.reply({ content: 'OK' });
                info.client.channels.resolve(info.channelID).send({ files: [url] });
            },
            integration_types: [0, 1],
            contexts: [0, 1, 2]
        }
    ];

function GetChaosName (bot, info, character)
{
    character.ChaosName = character.ChaosName || "CHAOS";
    let name = character.ChaosName;
    bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
    for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
    {
        let module = rpmodules[bot.data[info.serverId].modules[i]];
        bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
        let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

        if (module && module.overwriteChaosName)
            name = module.overwriteChaosName(mdata, bot, character, info, name);

        bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
    }

    return name;
}

function CheckIntegrity (bot, info)
{
    bot.data[info.serverId] = bot.data[info.serverId] || { characters: {} };
    bot.data[info.serverId].characters = bot.data[info.serverId].characters || {};

    bot.data[info.serverId].combat = bot.data[info.serverId].combat || {};
    bot.data[info.serverId].combat[info.channelID] = bot.data[info.serverId].combat[info.channelID] || { tracker: null, characters: [] };
}

function rand (min, max)
{
    return min + (Math.floor(rando() * (max - min)));
}

function shuffle (a)
{
    var j, x, i;
    for (i = a.length - 1; i > 0; i--)
    {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function CapitalizeFirstLetter (str)
{
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function GetStatNum (character, statname)
{
    if (!character.SUPER)
    {
        return character[statname];
    }
    else
    {
        return character[statname] * 2;
    }
}

function RollStat (statNum)
{
    return RollMod(GetStatMod(statNum));
}

function GetStatMod (statNum)
{
    return ((statNum - 10) / 2);
}

function RollMod (statMod, boost = 2)
{
    return Math.round(wrand(1, 20, boost)) + statMod;
}

function HealthBar (current, max)
{
    var str = "";

    for (var i = 0; i < current; i++)
        str += "▰";

    for (var i = 0; i < max - current; i++)
        str += "▱";

    return str;
}

function StatBar (current, bonus, max)
{
    var str = "";

    for (var i = 0; i < current; i++)
        str += "◆";

    for (var i = 0; i < bonus; i++)
        str += "◈";

    for (var i = 0; i < max - (current + bonus); i++)
        str += "◇";

    return str;
}

function ChaosBar (current, max)
{
    var str = "";

    for (var i = 0; i < current; i++)
        str += "★";

    for (var i = 0; i < max - current; i++)
        str += "☆";

    return str;
}

function GetMaxHP (character)
{
    var bonus = 1;

    if (GetCharacterRank(character) == "S")
        bonus = 2;

    return Math.ceil(GetStatNum(character, "CON") * 1.8 + character.Emeralds) * bonus;
}

function GetMaxCHAOS (character)
{
    var bonus = 0;

    if (GetCharacterRank(character) == "S")
        bonus = 10;

    if (character.CLASS == "Savant")
        return Math.ceil(((GetStatNum(character, "INT") / 2 + GetStatNum(character, "CHA") / 2) + character.Emeralds) * 0.7) + bonus;
    else if (character.CLASS == "Mystic")
        return Math.ceil((GetStatNum(character, "CHA") + character.Emeralds + character.RESOURCE) * 0.7) + bonus;
    else
        return Math.ceil((GetStatNum(character, "CHA") + character.Emeralds) * 0.7) + bonus;
}

function GetAC (character)
{
    var ward = 0;
    if (character.WARD)
    {
        ward = character.WARD;
    }

    if (GetCharacterRank(character) == "S")
        ward += 5;

    if (character.CLASS == "Dasher")
        return Math.ceil(11 + GetStatMod(GetStatNum(character, "DEX") + character.Emeralds) + (character.CHAOS / 3)) + ward;
    else if (character.CLASS == "Bruiser")
        return Math.ceil(10 + GetStatMod(GetStatNum(character, "DEX") + character.Emeralds) + character.RESOURCE) + ward;
    else
        return Math.ceil(10 + GetStatMod(GetStatNum(character, "DEX") + character.Emeralds)) + ward;
}

function GetToHitMod (character)
{
    if (character.CLASS == "Bruiser")
        return GetStatMod(GetStatNum(character, "STR") + character.Emeralds);
    else if (character.CLASS == "Dasher")
        return GetStatMod(GetStatNum(character, "DEX") + character.Emeralds);
    else if (character.CLASS == "Oracle")
        return GetStatMod(GetStatNum(character, "CHA") + character.Emeralds);
    else if (character.CLASS == "Mystic")
        return GetStatMod(GetStatNum(character, "INT") + character.Emeralds) + (character.CHAOS / 2);
    else if (character.CLASS == "Savant")
        return GetStatMod(GetStatNum(character, "INT") + character.Emeralds);
    else if (character.CLASS == "Fighter")
        return character.RESOURCE + character.Emeralds + 1;
    else
        return GetStatMod(GetStatNum(character, "STR") + character.Emeralds);
}

function DamageRoll (character, multiplier = 1)
{
    var max = 4 + multiplier;

    if (GetCharacterRank(character) == "B")
        max = 5 + multiplier;
    if (GetCharacterRank(character) == "A")
        max = 6 + multiplier;
    if (GetCharacterRank(character) == "S")
        max = 7 + multiplier;

    var boost = 2;

    if (character.BUFF)
    {
        boost = 3;
        max *= 2;
    }

    if (character.CLASS == "Dasher")
        boost += character.RESOURCE / 3;

    if (character.CLASS == "Fighter")
        return Math.ceil(Math.max(1, wrand(1, max, boost) + Math.max(0, Math.max(GetStatMod(GetStatNum(character, "DEX")), GetStatMod(GetStatNum(character, "STR") + character.Emeralds)) / 3)) + (character.CHAOS / 4));
    else if (character.CLASS == "Mystic")
        return Math.ceil(Math.max(1, wrand(1, max, boost) + Math.max(0, GetStatMod(GetStatNum(character, "CHA") + character.Emeralds) / 3)));
    else if (character.CLASS == "Savant")
        return Math.ceil(Math.max(1, wrand(1, max, boost) + Math.max(0, GetStatMod(GetStatNum(character, "INT") + character.Emeralds) / 4)));
    else
        return Math.ceil(Math.max(1, wrand(1, max, boost) + Math.max(0, GetStatMod(GetStatNum(character, "STR") + character.Emeralds) / 5)));
}

function wrand (min, max, weight)
{
    var r = rando();
    var rr = 1 - Math.pow(r, weight);
    return min + (Math.floor(rr * (max - min)));
}

function CheckStat (character, stat)
{
    switch (stat)
    {
        case "DEX": return CheckDEX(character);
        case "CON": return CheckCON(character);
        case "STR": return CheckSTR(character);
        case "INT": return CheckINT(character);
        case "CHA": return CheckCHA(character);
        case "RANK": return DamageRoll(character);
    }
}

function CheckDEX (character)
{
    return RollMod(GetStatMod(GetStatNum(character, "DEX") + character.Emeralds));
}

function CheckCON (character)
{
    return RollMod(GetStatMod(GetStatNum(character, "CON") + character.Emeralds));
}

function CheckSTR (character)
{
    return RollMod(GetStatMod(GetStatNum(character, "STR") + character.Emeralds));
}

function CheckINT (character)
{
    return RollMod(GetStatMod(GetStatNum(character, "INT") + character.Emeralds));
}

function CheckCHA (character)
{
    var result = RollMod(GetStatMod(GetStatNum(character, "CHA") + character.Emeralds));

    return Math.round(result);
}

function GetClassResourceName (className)
{
    switch (className)
    {
        case "Fighter": return "Pressure";
        case "Bruiser": return "Iron Will";
        case "Dasher": return "Momentum";
        case "Mystic": return "Manaward";
        case "Savant": return "Ingenuity";
        case "Oracle": return "Spirit";
    }

    return "Energy";
}

function CapResource (character)
{
    switch (character.CLASS)
    {
        case "Fighter": character.RESOURCE = Math.max(0, Math.min(character.RESOURCE, MaxResource(character))); break;
        case "Bruiser": character.RESOURCE = Math.max(0, Math.min(character.RESOURCE, MaxResource(character))); break;
        case "Dasher": character.RESOURCE = Math.max(0, Math.min(character.RESOURCE, MaxResource(character))); break;
        case "Mystic": character.RESOURCE = Math.max(0, Math.min(character.RESOURCE, MaxResource(character))); break;
        case "Savant": character.RESOURCE = Math.max(0, Math.min(character.RESOURCE, MaxResource(character))); break;
        case "Oracle": character.RESOURCE = Math.max(0, Math.min(character.RESOURCE, MaxResource(character))); break;
    }
}

function UpdateInitiativeTracker (bot, info, tracker)
{
    if (tracker)
    {
        if (tracker.modern)
        {
            info.client.channels.fetch(tracker.channelId).then(channel =>
            {
                let msgId = bot.data[info.serverId].combat[tracker.channelId].tracker;

                channel.messages.fetch(msgId).then(message =>
                {
                    message.edit(GenerateInitiativeList(bot, info, bot.data[info.serverId].combat[tracker.channelId].characters));
                });
            });
        }
        else if (tracker.channelId && tracker.msgId)
        {
            info.client.channels.fetch(tracker.channelId).then(channel =>
            {
                channel.messages.fetch(tracker.msgId).then(message =>
                {
                    message.edit(GenerateInitiativeList(bot, info, tracker.characters));
                });
            });
        }
    }
    else
    {
        console.log("Tracker broken:", tracker);
    }
}

function GenerateInitiativeList (bot, info, defenders, initial = false)
{
    if (defenders.length <= 0)
        return "```\nNo combatants have entered yet!\nRoll /initiative to join the fight!\n```";

    defenders = defenders.sort(function (a, b)
    {
        an = bot.data[info.serverId].characters[a.Name.toUpperCase()];
        bn = bot.data[info.serverId].characters[b.Name.toUpperCase()];

        let al = a.hasLeft ? a.last : an.lastInitiative;
        let bl = b.hasLeft ? b.last : bn.lastInitiative;

        return bl - al;
    });

    let msg = "```diff\n";

    for (let i = 0; i < defenders.length; i++)
    {
        console.log("Handling", defenders[i]);
        let charRef = bot.data[info.serverId].characters[defenders[i].Name.toUpperCase()];

        let prefix = (charRef.HP <= 0 ? "-" : "+");

        if ((!charRef.tracker || charRef.tracker.leaving) && !initial)
            prefix = "~";

        let initiativeNumber = charRef.lastInitiative;

        if (defenders[i].hasLeft)
        {
            prefix = "~";
            initiativeNumber = defenders[i].last;
        }

        let statusStr = "♥" + charRef.HP + "/" + GetMaxHP(charRef) + "  " + "★" + charRef.CHAOS + "/" + GetMaxCHAOS(charRef);
        msg += prefix + " [" + (initiativeNumber < 10 ? "0" : "") + initiativeNumber + "] " + charRef.Name + ": " + (charRef.HP <= 0 ? " (Defeated)" : statusStr) + (prefix == "~" ? " (Left Combat)" : "") + "\n";
    }

    msg += "```";

    return msg;
}

function MaxResource (character)
{
    var bonus = 1;

    if (GetCharacterRank(character) == "S")
        bonus = 2;

    switch (character.CLASS)
    {
        case "Fighter": return (((character.STR + character.DEX) / 2) / 1.5) * bonus;
        case "Oracle": return (character.CHA / 1.2) * bonus;
        case "Bruiser": return (character.CON / 2) * bonus;
        case "Dasher": return (character.DEX / 2) * bonus;
        case "Mystic": return (character.INT / 2) * bonus;
        case "Savant": return 3;
    }
}

function ClassBar (character)
{
    var onc = "";
    var offc = "";
    var max = MaxResource(character);

    if (character.CLASS == "Savant")
    {
        switch (character.RESOURCE)
        {
            case 0: return "𒀸";
            case 1: return "𒀪";
            case 2: return "𒀫";
            case 3: return "𒀬 READY";
            case 4: return "𒀬 READY";
        }
    }

    switch (character.CLASS)
    {
        case "Fighter": onc = "🜋"; offc = "🜊"; break;
        case "Bruiser": onc = "⛊"; offc = "⛉"; break;
        case "Dasher": onc = "►"; offc = "▷"; break;
        case "Mystic": onc = "✥"; offc = "✢"; break;
        case "Oracle": onc = "✿"; offc = "❀"; break;
    }

    //🜋🜊 ⍟⌾
    var str = "";

    for (var i = 0; i < character.RESOURCE; i++)
        str += onc;

    for (var i = 0; i < max - character.RESOURCE; i++)
        str += offc;

    return str;
}

function GetCharacterRank (character)
{
    var total = character.DEX + character.CON + character.STR + character.INT + character.CHA;

    if (total < 55)
        return "C";
    if (total < 60)
        return "B";
    if (total < 65)
        return "A";

    return "S";
}

function GetCharacterRankDisplay (character)
{
    var total = character.DEX + character.CON + character.STR + character.INT + character.CHA;

    if (total < 50)
        return "C-" + (50 - total);
    if (total == 50)
        return "C";
    if (total < 55)
        return "C+" + (total - 50);
    if (total == 55)
        return "B";
    if (total < 60)
        return "B+" + (total - 55);
    if (total == 60)
        return "A";
    if (total < 65)
        return "A+" + (total - 60);

    return "S";
}

function GetCharacterDetailForm (bot, info, character)
{
    var total = character.DEX + character.CON + character.STR + character.INT + character.CHA;

    var emeralds = "";
    character.Emeralds = character.Emeralds || 0;

    for (var i = 0; i < character.Emeralds; i++)
        emeralds += "💎";

    return GetCharacterRankDisplay(character) + " Rank " + character.CLASS
        + (character.Emeralds > 0 ? "\Power Bonus: " + emeralds : "")
        + (character.SUPER ? "\n►► **SUPER MODE** ◄◄" : "")
        + (character.BOOST ? "\n✿" + character.BOOST : "")
        + (character.BUFF ? "\n𒀬" + character.BUFF : "")
        + "\n` AC`: " + GetAC(character) + (character.WARD ? "✥ (Ward remaining: " + character.WARD + ")" : "")
        + "\n` HP`: " + HealthBar(character.HP, GetMaxHP(character))
        + "\n`" + GetClassResourceName(character.CLASS) + "`: " + ClassBar(character)
        + "\n`STR`: " + StatBar(GetStatNum(character, "STR"), character.Emeralds, 20)
        + "\n`DEX`: " + StatBar(GetStatNum(character, "DEX"), character.Emeralds, 20)
        + "\n`CON`: " + StatBar(GetStatNum(character, "CON"), character.Emeralds, 20)
        + "\n`INT`: " + StatBar(GetStatNum(character, "INT"), character.Emeralds, 20)
        + "\n`CHA`: " + StatBar(GetStatNum(character, "CHA"), character.Emeralds, 20)
        + "\n`" + GetChaosName(bot, info, character) + "`: " + ChaosBar(character.CHAOS, GetMaxCHAOS(character))
        + "\n" + total + " points assigned in total.";
}

function PerformAttack (bot, info, attacker, defenders, chaos = false)
{
    var outputs = [];
    var defstr = "";
    var attackResourceDone = false;

    console.log(defenders);

    if (defenders.length == 1)
    {
        defstr = defenders[0].Name;
    }
    else
    {
        for (var i = 0; i < defenders.length; i++)
        {
            defstr += (i == 0 ? "" : (i == defenders.length - 1 ? " and " : ", ")) + defenders[i].Name;
        }
    }

    var buff = false;
    if (attacker.BUFF && attacker.BUFF > 0)
        buff = true;

    if (attacker.CLASS == "Oracle")
        attacker.RESOURCE--;

    if (attacker.CLASS == "Savant")
    {
        attacker.RESOURCE++;
    }

    var wasBuff = buff ? "𒀬" : "";

    CapResource(attacker);

    var wasResource = attacker.RESOURCE;
    var allmiss = true;

    for (var i = 0; i < defenders.length; i++)
    {
        var defender = defenders[i];
        var boost = 2;

        if (attacker.BOOST)
        {
            boost += attacker.BOOST;
            attacker.BOOST--;

            if (attacker.BOOST <= 0)
                delete attacker.BOOST;
        }

        if (attacker.CLASS == "Dasher")
            boost = 1.5 + (attacker.RESOURCE / 8);

        var attackRoll = Math.ceil(RollMod(GetToHitMod(attacker) + (chaos ? attacker.CHAOS : 0), boost));
        var defendRoll = GetAC(defender);

        var dmgboost = 1;
        if (attacker.CLASS == "Dasher")
            dmgboost += attacker.RESOURCE / 6;

        let dmg = Math.ceil((DamageRoll(attacker, dmgboost) + (chaos ? Math.ceil(attacker.CHAOS / 2) : 0)) / Math.max(1, defenders.length * 0.7));
        dmg = Math.max(1, dmg);

        let aExtraLines = [];
        let dExtraLines = [];

        let hit = attackRoll >= defendRoll;

        bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
        for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
        {
            let module = rpmodules[bot.data[info.serverId].modules[i]];
            bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
            let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

            let response = module.onAttack && module.onAttack(mdata, bot, attacker, info, defender, chaos, hit, dmg);
            if (response.wasChaos === true || response.wasChaos === false)
                chaos = response.wasChaos;

            if (response.hit === true)
            {
                hit = true;
                attackRoll = Math.max(attackRoll, defendRoll);
            }
            if (response.hit === false)
            {
                hit = false;
                attackRoll = Math.min(attackRoll, defendRoll - 1);
            }
            if (response.damage)
                dmg = response.damage;

            if (response.extraText)
                aExtraLines.push(response.extraText);

            response = module.onDefend && module.onDefend(mdata, bot, defender, info, attacker, chaos, hit, dmg);
            if (response.wasChaos === true || response.wasChaos === false)
                chaos = response.wasChaos;

            if (response.hit === true)
            {
                hit = true;
                attackRoll = Math.max(attackRoll, defendRoll);
            }
            if (response.hit === false)
            {
                hit = false;
                attackRoll = Math.min(attackRoll, defendRoll - 1);
            }
            if (response.damage)
                dmg = response.damage;

            if (response.extraText)
                dExtraLines.push(response.extraText);

            bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
        }

        outputs[0] = {
            name: attacker.Name, avatar: attacker.Avatar, text: chaos ? (attacker.Name + " performs a " + GetChaosName(bot, info, attacker) + " " + wasBuff + "attack on " + defstr + "!\n"
                + GetChaosName(bot, info, attacker) + ": " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) + " \n " + GetClassResourceName(attacker.CLASS) + ": " + ClassBar(attacker))
                : (attacker.Name + " performs an " + wasBuff + "attack on " + defstr + "!\n"
                    + GetClassResourceName(attacker.CLASS) + ": " + ClassBar(attacker)), title: chaos ? GetChaosName(bot, info, attacker) + " attack!" : "Attack!"
        };

        if (aExtraLines.length > 0)
            outputs[0].text += "\n" + aExtraLines.join("\n");

        if (defender.CLASS == "Bruiser" && hit)
            defender.RESOURCE++;

        attacker.prevAttackDidFail = false;

        if (attacker.CLASS == "Dasher")
        {
            if (attackRoll > defendRoll)
                attacker.RESOURCE += Math.ceil((attackRoll - defendRoll) / 5);
            else
                attacker.RESOURCE -= Math.ceil((defendRoll - attackRoll) / 5); //split two ways so I can ceil in both directions
        }

        if (hit)
        {
            allmiss = false;

            if (defender.CLASS == "Bruiser")
            {
                dmg -= Math.ceil(defender.CHAOS / 3);
            }

            if (attacker.CLASS == "Mystic")
                attacker.RESOURCE += Math.ceil(dmg / 2);

            if (attacker.CLASS == "Fighter")
            {
                attacker.RESOURCE--;
            }

            if (attacker.CLASS == "Bruiser")
                attacker.RESOURCE--;

            dmg = Math.max(1, dmg);

            var trueDmg = dmg;

            defender.HP = Math.max(0, defender.HP - dmg);
            UpdateInitiativeTracker(bot, info, defender.tracker);

            defender.CHAOS = Math.min(defender.CHAOS + Math.ceil(dmg / 2), GetMaxCHAOS(defender));

            if (chaos)
                attacker.CHAOS = 0;

            let text = defender.Name + " takes " + dmg + " point" + (dmg == 1 ? "" : "s") + " of damage! (" + attackRoll + (attacker.BOOST ? "✿" : "") + " vs AC" + defendRoll + (defender.WARD ? "✥" : "") + ")\n" + "HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender)) + " \n " + GetChaosName(bot, info, defender) + ": " + ChaosBar(defender.CHAOS, GetMaxCHAOS(defender)) + " \n " + GetClassResourceName(defender.CLASS) + ": " + ClassBar(defender);
            if (dExtraLines.length > 0)
                text += "\n" + dExtraLines.join("\n");

            outputs.push({ title: "Damage!", name: defenders[i].Name, avatar: defender.Avatar, text: text });
        }
        else
        {
            if (attacker.CLASS == "Fighter")
            {
                if (attacker.RESOURCE == 0)
                    attacker.RESOURCE = 3;
                else
                    attacker.RESOURCE++;
            }

            let text = defender.Name + " avoided taking damage! (" + attackRoll + (attacker.BOOST ? "✿" : "") + " vs AC" + defendRoll + (defender.WARD ? "✥" : "") + ")";
            if (dExtraLines.length > 0)
                text += "\n" + dExtraLines.join("\n");

            outputs.push({ title: "Damage Avoided!", name: defenders[i].Name, avatar: defender.Avatar, text: text });

            if (chaos)
                attacker.CHAOS = 0;

        }
        if (defender.WARD)
        {
            defender.WARD--;

            if (defender.WARD <= 0)
                delete defender.WARD;
        }

        CapResource(attacker);
        CapResource(defender);

        if (chaos)
            attacker.CHAOS = 0;
    }

    if (attacker.BUFF)
    {
        attacker.BUFF--;

        if (attacker.BUFF <= 0)
            delete attacker.BUFF;
    }
    var showChaos = false;
    if (allmiss)
    {
        attacker.CHAOS = Math.min(attacker.CHAOS + 1, GetMaxCHAOS(attacker));
        showChaos = true;
    }

    if (wasResource != attacker.RESOURCE || showChaos)
        outputs.push({ title: "", name: attacker.Name, avatar: attacker.Avatar, text: (showChaos ? GetChaosName(bot, info, attacker) + ": " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) : "") + ((wasResource != attacker.RESOURCE ? (showChaos ? " \n " : "") + GetClassResourceName(attacker.CLASS) + ": " + ClassBar(attacker) : "")) });

    return outputs;
}

function PerformHeal (bot, info, attacker, defenders)
{
    var outputs = [];
    var defstr = "";

    if (defenders.length == 1)
    {
        defstr = defenders[0].Name;
    }
    else
    {
        for (var i = 0; i < defenders.length; i++)
        {
            defstr += (i == 0 ? "" : (i == defenders.length - 1 ? " and " : ", ")) + defenders[i].Name;
        }
    }

    var buff = false;
    if (attacker.BUFF && attacker.BUFF > 0)
        buff = true;

    var wasBuff = buff ? "𒀬" : "";

    outputs[0] = {
        title: "Heal!", name: attacker.Name, avatar: attacker.Avatar, text: attacker.Name + " " + wasBuff + "heals " + defstr + "!\n"
            + GetClassResourceName(attacker.CLASS) + ": " + ClassBar(attacker)
    };

    for (var i = 0; i < defenders.length; i++)
    {
        var defender = defenders[i];

        let aExtraLines = [];
        let dExtraLines = [];

        var dmg = Math.floor(DamageRoll(attacker) / defenders.length);

        dmg = Math.max(0, dmg);

        bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
        for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
        {
            let module = rpmodules[bot.data[info.serverId].modules[i]];
            bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
            let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

            let response = module.onHeal && module.onHeal(mdata, bot, attacker, info, defender, dmg);
            if (response.healedPoints)
                dmg = response.healedPoints;
            if (response.extraText)
                aExtraLines.push(response.extraText);
            response = module.onHealed && module.onHealed(mdata, bot, defender, info, attacker, dmg);
            if (response.extraText)
                dExtraLines.push(response.extraText);
            if (response.healedPoints)
                dmg = response.healedPoints;

            bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
        }

        if (aExtraLines.length > 0)
            outputs[0].text += "\n" + aExtraLines.join("\n");

        var trueDmg = dmg;

        defender.HP = Math.min(GetMaxHP(defender), defender.HP + dmg);

        UpdateInitiativeTracker(bot, info, defender.tracker);

        let text = defender.Name + " heals for " + dmg + " point" + (dmg == 1 ? "" : "s") + "!\n" + "HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender));
        if (dExtraLines.length > 0)
            text += "\n" + dExtraLines.join("\n");
        outputs.push({ title: "Restored:", name: defenders[i].Name, avatar: defender.Avatar, text: text });

        CapResource(attacker);
        CapResource(defender);
    }

    if (attacker.BUFF)
    {
        attacker.BUFF--;

        if (attacker.BUFF <= 0)
            delete attacker.BUFF;
    }

    return outputs;
}

function PerformWard (bot, info, attacker, defenders)
{
    var outputs = [];
    var defstr = "";

    if (defenders.length == 1)
    {
        defstr = defenders[0].Name;
    }
    else
    {
        for (var i = 0; i < defenders.length; i++)
        {
            defstr += (i == 0 ? "" : (i == defenders.length - 1 ? " and " : ", ")) + defenders[i].Name;
        }
    }

    var buff = false;
    if (attacker.BUFF && attacker.BUFF > 0)
        buff = true;

    var wasBuff = buff ? "𒀬" : "";

    outputs[0] = {
        title: "Ward!", name: attacker.Name, avatar: attacker.Avatar, text: attacker.Name + " " + wasBuff + "wards " + defstr + "!\n"
            + GetClassResourceName(attacker.CLASS) + ": " + ClassBar(attacker)
    };

    for (var i = 0; i < defenders.length; i++)
    {
        var defender = defenders[i];

        var dmg = Math.floor(DamageRoll(attacker) / defenders.length);

        dmg = Math.max(0, dmg);

        let aExtraLines = [];
        let dExtraLines = [];
        bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
        for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
        {
            let module = rpmodules[bot.data[info.serverId].modules[i]];
            bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
            let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

            let response = module.onWard && module.onWard(mdata, bot, attacker, info, defender, dmg);
            if (response.strength)
                dmg = response.strength;
            if (response.extraText)
                aExtraLines.push(response.extraText);
            response = module.onWarded && module.onWarded(mdata, bot, defender, info, attacker, dmg);
            if (response.extraText)
                dExtraLines.push(response.extraText);
            if (response.strength)
                dmg = response.strength;

            bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
        }
        if (aExtraLines.length > 0)
            outputs[0].text += "\n" + aExtraLines.join("\n");

        var trueDmg = dmg;

        let text = "";

        if (!defender.WARD || defender.WARD < dmg)
        {
            defender.WARD = dmg;
            text = defender.Name + " receives a ward with a strength of " + dmg + " point" + (dmg == 1 ? "" : "s") + "!";
        }
        else
        {
            text = defender.Name + " could not receive more wards!";
        }

        if (dExtraLines.length > 0)
            text += "\n" + dExtraLines.join("\n");
        outputs.push({ title: "Effect:", name: defenders[i].Name, avatar: defender.Avatar, text: text });

        CapResource(attacker);
        CapResource(defender);
    }

    if (attacker.BUFF)
    {
        attacker.BUFF--;

        if (attacker.BUFF <= 0)
            delete attacker.BUFF;
    }

    if (attacker.CLASS == "Oracle")
    {
        attacker.RESOURCE--;
        outputs.push({ title: GetClassResourceName(attacker.CLASS), name: attacker.Name, avatar: attacker.Avatar, text: GetClassResourceName(attacker.CLASS) + ": " + ClassBar(attacker) });
    }
    return outputs;
}

function PerformBoost (bot, info, attacker, defenders)
{
    var outputs = [];
    var defstr = "";

    if (defenders.length == 1)
    {
        defstr = defenders[0].Name;
    }
    else
    {
        for (var i = 0; i < defenders.length; i++)
        {
            defstr += (i == 0 ? "" : (i == defenders.length - 1 ? " and " : ", ")) + defenders[i].Name;
        }
    }

    var buff = false;
    if (attacker.BUFF && attacker.BUFF > 0)
        buff = true;

    var wasBuff = buff ? "𒀬" : "";

    outputs[0] = {
        title: "Boost!", name: attacker.Name, avatar: attacker.Avatar, text: attacker.Name + " " + wasBuff + "boosts " + defstr + "!\n"
            + GetClassResourceName(attacker.CLASS) + ": " + ClassBar(attacker)
    };

    for (var i = 0; i < defenders.length; i++)
    {
        var defender = defenders[i];

        var dmg = Math.floor(DamageRoll(attacker, 1 + (attacker.CHAOS / 6)) / defenders.length);

        dmg = Math.max(0, dmg);

        let aExtraLines = [];
        let dExtraLines = [];
        bot.data[info.serverId].modules = bot.data[info.serverId].modules || [];
        for (let i = 0; i < bot.data[info.serverId].modules.length; i++)
        {
            let module = rpmodules[bot.data[info.serverId].modules[i]];
            bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
            let mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] || {};

            let response = module.onBoost && module.onBoost(mdata, bot, attacker, info, defender, dmg);
            if (response.strength)
                dmg = response.strength;
            if (response.extraText)
                aExtraLines.push(response.extraText);
            response = module.onBoosted && module.onBoosted(mdata, bot, defender, info, attacker, dmg);
            if (response.extraText)
                dExtraLines.push(response.extraText);
            if (response.strength)
                dmg = response.strength;

            bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
        }
        if (aExtraLines.length > 0)
            outputs[0].text += "\n" + aExtraLines.join("\n");

        var trueDmg = dmg;

        let text = "";

        if (!defender.BOOST || defender.BOOST < dmg)
        {
            defender.BOOST = dmg;
            text = defender.Name + " receives a boost with a strength of " + dmg + " point" + (dmg == 1 ? "" : "s") + "!";
        }
        else
        {
            text = defender.Name + " couldn't be boosted any further!";
        }

        if (dExtraLines.length > 0)
            text += "\n" + dExtraLines.join("\n");
        outputs.push({ title: "Effect:", name: defenders[i].Name, avatar: defender.Avatar, text: text });

        CapResource(attacker);
        CapResource(defender);
    }

    if (attacker.BUFF)
    {
        attacker.BUFF--;

        if (attacker.BUFF <= 0)
            delete attacker.BUFF;
    }

    attacker.RESOURCE += GetStatMod(GetStatNum(attacker, "CHA"));
    CapResource(attacker);

    outputs.push({ title: "", name: attacker.Name, avatar: attacker.Avatar, text: GetClassResourceName(attacker.CLASS) + ": " + " " + ClassBar(attacker) });

    return outputs;
}

function RecursivePost (bot, info, lineDatas)
{
    var lineData = lineDatas.splice(0, 1)[0];
    console.log(lineData);
    if (!lineData)
        lineData = { name: "RP Fight Bot" };

    bot.PostWebhook(bot, info.serverId, info.channelID, lineData.name, lineData.avatar, lineData.text, function ()
    {
        if (lineDatas.length > 0)
        {
            setTimeout(function () { RecursivePost(bot, info, lineDatas); }, 1000);
        }
    }, true, lineData.title);
}

String.prototype.toColor = function ()
{
    var hash = 0;
    if (this.length === 0) return hash;
    for (var i = 0; i < this.length; i++)
    {
        hash = this.charCodeAt(i) + ((hash << 5) - hash);
        hash = hash & hash;
    }
    var color = '0x';
    for (var i = 0; i < 3; i++)
    {
        var value = (hash >> (i * 8)) & 255;
        color += ('00' + value.toString(16)).substr(-2);
    }

    return eval(color);
};

String.prototype.toRGB = function ()
{
    var hash = 0;
    if (this.length === 0) return hash;
    for (var i = 0; i < this.length; i++)
    {
        hash = this.charCodeAt(i) + ((hash << 5) - hash);
        hash = hash & hash;
    }
    var rgb = [0, 0, 0];
    for (var i = 0; i < 3; i++)
    {
        var value = (hash >> (i * 8)) & 255;
        rgb[i] = value;
    }
    return `${rgb[0]}${rgb[1]}${rgb[2]}`;
};
