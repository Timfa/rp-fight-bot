console.log("STARTING");
console.log("--------------------------------------------------");

//var Discord = require('discord.io');
const { REST, Routes } = require('discord.js');

var auth = require('./auth.json');
var fs = require('fs');
const axios = require("axios");
var rpmodules = require('./rp-modules');

var cmdsFile = require('./commands.js');

console.log("Requires loaded in.");

var commands = cmdsFile.cmds;
var admins = cmdsFile.admins;
console.log(commands.length + " Commands loaded");

/*var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});*/
var bot = {};

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
var slashcommands = [
];

for (let i = 0; i < commands.length; i++)
{
    slashcommands.push(
        {
            name: commands[i].cmd,
            description: (commands[i].hidden ? "(ADMIN ONLY) " : "") + commands[i].params,
            options: commands[i].options || []
        }
    );
}

let modules = Object.keys(rpmodules);

for (let m = 0; m < modules.length; m++)
{
    let moduleName = modules[m];
    let module = rpmodules[moduleName];

    if (!module.commands)
        continue;

    for (var i = 0; i < module.commands.length; i++)
    {
        slashcommands.push(
            {
                name: module.commands[i].cmd,
                description: (module.commands[i].hidden ? "(Module; ADMIN ONLY) " : "(" + moduleName + " Module) ") + module.commands[i].params,
                options: module.commands[i].options || []
            }
        );
    }
}

const rest = new REST({ version: '10' }).setToken(auth.token);

(async () =>
{
    try
    {
        console.log('Started refreshing application (/) commands.');

        await rest.put(Routes.applicationCommands(auth.id), { body: slashcommands });

        console.log('Successfully reloaded application (/) commands.');
    } catch (error)
    {
        console.error(error);
    }
})();

const { Client, GatewayIntentBits } = require('discord.js');
const client = new Client({
    intents:
        [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.MessageContent,
            GatewayIntentBits.GuildMessages,
            GatewayIntentBits.GuildMembers
        ]
});

client.on('interactionCreate', async interaction =>
{
    if (!interaction.isChatInputCommand()) return;
    console.log(interaction);
    let arg = interaction.options && interaction.options.data.length > 0 && interaction.options.data[0].value;
    onMessage(interaction.user, interaction.user.id, interaction.channelId, "%" + interaction.commandName + " " + arg,
        { //evt
            d:
            {
                author: interaction.user,
                guild_id: interaction.guildId
            },

            interaction: interaction
        }, interaction.options && interaction.options.data, true);
});

client.login(auth.token);

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
console.log("initializing...");

var initialized = false;
bot.commands = commands;

bot.data = null;
bot.lastSave = Date.now();

bot.saveBackup = function (silent = false)
{
    console.log("Writing Backup");
    fs.writeFile("./botDataBackup.json", JSON.stringify(bot.data), function (err)
    {
        if (err)
        {
            console.log(err);
        }
        else
        {
            if (!silent)
            {
                bot.sendMessage({
                    to: bot.data.lastServer,
                    message: "Bot data back-up created.",
                    typing: false
                });
            }
        }
    });
};

bot.saveData = function (callback, context)
{
    console.log("Saving...");
    try
    {
        if (bot.data != null && bot.data != {} && initialized)
        {
            bot.data = bot.data || {};

            fs.writeFile("./botData.json", JSON.stringify(bot.data), function (err)
            {
                if (err)
                {
                    console.log(err);
                }

                console.log("Saved.");

                context = context || this;
                callback && callback.call(context, true);
            });
        }
    }
    catch (ex)
    {
        console.log("Error while saving!");
        console.log(ex);
        context = context || this;
        callback && callback.call(context, false);
    }
};

console.log("savedata function defined");

client.on('ready', () =>
{
    console.log(`Logged in as ${client.user.tag}!`);

    fs.readFile('./botData.json', function read (err, data) 
    {
        if (err) 
        {
            data = "{}";
        }
        try
        {
            bot.data = JSON.parse(data);
        }
        catch (e)
        {
            if (bot.data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if (bot.data == null || bot.data == {})
        {
            fs.readFile('./botDataBackup.json', function read (err, data)
            {
                if (err)
                {
                    data = "{}";
                }
                try
                {
                    bot.data = JSON.parse(data);
                    console.log("Backup restored");

                    if (bot.data.lastServer)
                    {
                        bot.sendMessage({
                            to: bot.data.lastServer,
                            message: "A crash occurred, but I managed to restore a backup of my memory.",
                            typing: false
                        });
                        saveBackup = false;
                    }
                }
                catch (e)
                {
                    if (bot.data == null)
                    {
                        bot.data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;

                        if (bot.data.lastServer)
                        {
                            bot.sendMessage({
                                to: bot.data.lastServer,
                                message: "A crash occurred, and my memory file was lost :(",
                                typing: false
                            });
                        }
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            bot.saveBackup(true);

            initialized = true;
        }

        bot.data = bot.data || {};

        bot.data.servers = bot.data.servers || [];
        console.log("bup");
        for (var i = 0; i < bot.data.servers.length; i++)
        {
            bot.data[bot.data.servers[i]].adventurelock = [];
        }

        console.log("Checking if I need to send update message...");

        if (bot.data.update && bot.data.update != "no")
        {
            bot.sendMessage({
                to: bot.data.update,
                message: "Updated!",
                typing: false
            });

            bot.data.update = "no";
        }
        else
        {
            console.log("no update notifiy to do");
        }
    });
});

console.log("onready defined");

client.on("messageCreate", message =>
{
    console.log(message);
    onMessage(message.author, message.author.id, message.channelId, message.content, { client: client, d: { author: message.author, guild_id: message.guildId } });
});

onMessage = function (user, userID, channelID, message, evt, options, wasCommand)
{
    console.log(message);
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`

    if (!initialized)
        return;

    var wasBot = evt.d.author.bot;

    var id = evt.d.guild_id || userID;

    if (bot.data.servers.indexOf(id) < 0)
    {
        bot.data.servers.push(id);
    }

    var group = [userID];

    var words = message.split(' ');
    bot.data[id] = bot.data[id] || {};

    if (Date.now() > bot.lastSave + 3600000) // one hour
    {
        bot.saveBackup(true);
        bot.lastSave = Date.now();
    }

    bot.data[id].lastHuman = bot.data[id].lastHuman || {};

    if (!wasBot)
    {
        for (var i = 0; i < words.length; i++)
        {
            var allMentions = words[i].match(/(<@)!?([0-9]+)(>)/g);

            if (allMentions && allMentions.length > 0)
            {
                for (var m = 0; m < allMentions.length; m++)
                {
                    var validMention = allMentions[m].match(/(<@)!?([0-9]+)(>)/);
                    if (validMention && validMention[2] != bot.id)
                    {
                        if (group.indexOf(validMention[2]) < 0)
                        {
                            group.push(validMention[2]);
                        }
                    }
                }
            }
        }

        for (var i = 0; i < group.length; i++)
        {
            if (group[i] != userID)
            {
                bot.data[id].mentions = bot.data[id].mentions || {};

                bot.data[id].mentions[group[i]] = (bot.data[id].mentions[group[i]] || 0) + 1;
            }
        }

        bot.data[id].lastHuman[channelID] = userID;
    }
    else
    {
        bot.data[id].ignoredChannels = bot.data[id].ignoredChannels || {};
        bot.data[id].ignoredBots = bot.data[id].ignoredBots || {};

        if (!bot.data[id].ignoredChannels[channelID] && !bot.data[id].ignoredBots[evt.d.author.username.toLowerCase()])
        {
            bot.data[id].botLocations = bot.data[id].botLocations || {};
            bot.data[id].botLocations[evt.d.author.username.toLowerCase()] = { name: evt.d.author.username, place: "<#" + channelID + ">", user: bot.data[id].lastHuman[channelID] };
        }
    }

    bot.data[id].reminders = bot.data[id].reminders || {};
    bot.data[id].reminders[userID] = bot.data[id].reminders[userID] || [];

    for (let i = 0; i < bot.data[id].reminders[userID].length; i++)
    {
        let reminder = bot.data[id].reminders[userID][i];

        if (reminder && reminder.channel == channelID)
        {
            bot.data[id].reminders[userID].splice(i, 1);
            break;
        }
    }

    if (message.substring(0, 1) == '%' && wasCommand) 
    {
        var args = message.substring(1).split(' ');
        var cmd = args[0];

        var info = { client: client, user: user, userID: userID, channelID: channelID, message: message, evt: evt, serverId: id };

        bot.data.lastServer = channelID;

        args = args.splice(1);
        let found = false;
        for (var i = 0; i < commands.length; i++)
        {
            if (cmd.toLowerCase() == commands[i].cmd)
            {
                found = true;
                if (commands[i].hidden && admins.indexOf(info.userID) == -1)
                    return;

                commands[i].execute(bot, info, args, options);

                break;
            }

        }

        if (!found)
        {
            for (let m = 0; m < bot.data[info.serverId].modules.length; m++)
            {
                let moduleName = bot.data[info.serverId].modules[m];
                let module = rpmodules[moduleName];

                bot.data[info.serverId].moduleData = bot.data[info.serverId].moduleData || {};
                var mdata = bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[m]] || {};

                if (!module || !module.commands)
                    continue;

                for (var i = 0; i < module.commands.length; i++)
                {
                    if (cmd.toLowerCase() == module.commands[i].cmd)
                    {
                        found = true;
                        if (module.commands[i].hidden && admins.indexOf(info.userID) == -1)
                            return;

                        module.commands[i].execute(mdata, bot, info, args, options);
                        break;
                    }

                }

                bot.data[info.serverId].moduleData[bot.data[info.serverId].modules[i]] = mdata;
            }

            if (!found)
                interaction.reply({ content: 'Command not found! Perhaps a module is not loaded in this server?', ephemeral: true });
        }
    }

    bot.saveData();
};

bot.PostWebhook = function (bot, serverId, channelId, name, avatarUrl, text, callback, embed = false, title = "")
{
    if (!name)
        return;

    var displayname = name;
    var character = null;
    var color = null;

    if (embed)
    {
        character = bot.data[serverId].characters[displayname.toUpperCase().trim()];
        color = character.COLOR || displayname.toColor();
    }

    bot.WH(bot, channelId, displayname, text, avatarUrl, embed, embed, title, color, callback);

    client.user.setPresence({
        game: { name: "with " + displayname },
        status: 'online',
    });
};

bot.WH = async function (bot, channelId, displayname, text, avatarUrl, isEmbed, embed, title, color, callback)
{
    //idea: https://stackoverflow.com/questions/70631696/discord-webhook-post-to-channel-thread
    // is thread? get channel, make webhook in channel, pass threadID to webhook post
    let targetChannel = client.channels.resolve(channelId);
    let whChannel = targetChannel.isThread() ? targetChannel.parent : targetChannel;

    let send = function (wh)
    {
        let payload =
        {
            embeds: [
                {
                    title: title,
                    description: text,
                    color: color
                }
            ]
        };

        let payloadNonEmbed =
        {
            content: text
        };

        if (targetChannel.isThread())
        {
            payload.threadId = targetChannel.id;
            payloadNonEmbed.threadId = targetChannel.id;
        }

        wh.send(isEmbed ? payload : payloadNonEmbed).then(msg =>
        {
            callback && callback();
        });
    };

    let whName = displayname;
    const webhooks = await whChannel.fetchWebhooks();
    const webhook = webhooks.find(wh => wh.token);

    if (webhook)
    {
        if (webhook.avatar != avatarUrl || webhook.name != whName)
        {
            webhook.edit({ name: whName, avatar: avatarUrl }).then(wh => send(wh));
        }
        else
        {
            send(webhook);
        }
    }
    else
    {
        whChannel.createWebhook({
            name: whName,
            avatar: avatarUrl,
            reason: "Webhook for RP Fight Bot"
        }).then(wh => send(wh));
    }
};

String.prototype.toColor = function ()
{
    var hash = 0;
    if (this.length === 0) return hash;
    for (var i = 0; i < this.length; i++)
    {
        hash = this.charCodeAt(i) + ((hash << 5) - hash);
        hash = hash & hash;
    }
    var color = '0x';
    for (var i = 0; i < 3; i++)
    {
        var value = (hash >> (i * 8)) & 255;
        color += ('00' + value.toString(16)).substr(-2);
    }

    return eval(color);
};

String.prototype.toRGB = function ()
{
    var hash = 0;
    if (this.length === 0) return hash;
    for (var i = 0; i < this.length; i++)
    {
        hash = this.charCodeAt(i) + ((hash << 5) - hash);
        hash = hash & hash;
    }
    var rgb = [0, 0, 0];
    for (var i = 0; i < 3; i++)
    {
        var value = (hash >> (i * 8)) & 255;
        rgb[i] = value;
    }
    return `${rgb[0]}${rgb[1]}${rgb[2]}`;
};

bot.sendMessage = function (opts, callback)
{
    try
    {
        client.channels.resolve(opts.to).send(opts.message).then(msg =>
        {
            callback && callback(msg.url, { id: msg.id, message: msg });
        });
    }
    catch (e)
    {
        console.log("Error sending message: ", e);
    }
};

console.log("onmessage defined: awaiting [onready]");
